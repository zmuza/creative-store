<?php ?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8" />
	  <title><?= $pageTitle ?><?= $titleTrailing ?></title>
	  <meta name="description" content="<?= isset($metaDescription) ? $metaDescription : "Creativegg.com е мястото за креативни, оригинални и нестандартни подаръци. Тук ще намерите качествени подаръци, които изненадват, провокират, разсмиват и най-вече внасят повече усмивки в ежедневието."?>">
	  
	  <meta name="keywords" content="<?= isset($metaKeywords) ? $metaKeywords : "подаръци, podaruci, podaraci, уникални подаръци, подаръци за дома, подаръци за кухнята, подаръци за офиса, нестандартни подаръци, забавни подаръци, идеи за подаръци, креативните подаръци, интересни подаръци, щури подаръци, коледни подаръци"?>">
	  <meta name="viewport" content="width=device-width" />
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-39481037-3', 'creativegg.com');
      ga('send', 'pageview');
    </script>
	
    <script src="https://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js" async></script>
    <script type="text/javascript" src="<?=aref("scripts/communication.js")?>" async></script>
    <script type="text/javascript" src="<?=aref("scripts/common_ui.js")?>" async></script>
    <script type="text/javascript" src="<?=aref("scripts/common.js")?>" async></script>
    <script type="text/javascript" src="<?=aref("scripts/cart.js")?>" async></script>
<?php
if (isset($additionalJS) && is_array($additionalJS))
{
  foreach ($additionalJS as $jsEntry)
  {
?>
    <script type="text/javascript" src="<?=aref("scripts/$jsEntry")?>" async></script>
<?php
  }
}
?>
    <link rel="stylesheet" type="text/css" href="<?=aref("css/common.css")?>" /> 
    <link rel="stylesheet" type="text/css" href="<?=aref("css/common_product.css")?>" />
<?php
if (isset($additionalCSS) && is_array($additionalCSS))
{
  foreach ($additionalCSS as $cssEntry)
  {
?>
    <link rel="stylesheet" type="text/css" href="<?=aref("css/$cssEntry")?>" />
<?php
  }
}  

if ($blackFriday) { 
?>
    <link rel="stylesheet" type="text/css" href="<?=aref("css/blackfriday.css")?>" />
<?php
}
?>
    <style>
      *.hidden { display: none !important; }
    </style>
  </head>
  <body<? if (isset($bodyClass)) echo " class=\"$bodyClass\""; ?>>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1449150838638127";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <container>
      <top>
        <left>
          <t>Тел.: 0894 494 544</t>
        </left>
	      <right>
		      <a href="<?=aref("cart.php")?>" <?= classForItem('cart', 'current') ?>>
		        <t>Вашата количка:</t>
			      <cart_items></cart_items>
		      </a>
<?php /* 		      <a id="paymentLink" href="<?=aref("payment.php")?>" <?= classForItem('payment', 'current') ?>><t>Плащане</t></a> */ ?>
		    </right>
	    </top>
      <main>
	      <logo_strip>
	        <a href="<?=aref("")?>">
	          <img src="<?=aref("img/d/logo.png")?>">
	        </a>
	        <h1>Магазинът за креативни подаръци!</h1>
	      </logo_strip>
	      <ul id="menu_list">
          <li<?= classForItem('category0', 'current') ?>><a href="<?=aref("category.php")?>">всички</a></li>
          <li<?= classForItem('category1', 'current') ?>><a href="<?=aref("category.php?id=1")?>">за дома</a></li>
          <li<?= classForItem('category2', 'current') ?>><a href="<?=aref("category.php?id=2")?>">в кухнята</a></li>
          <li<?= classForItem('category3', 'current') ?>><a href="<?=aref("category.php?id=3")?>">в офиса</a></li>
          <li<?= classForItem('category4', 'current') ?>><a href="<?=aref("category.php?id=4")?>">за напреднали</a></li>
          <li<?= classForItem('category5', 'current') ?>><a href="<?=aref("category.php?id=5")?>">за деца</a></li>
          <li<?= classForItem('category6', 'current') ?>><a href="<?=aref("category.php?id=6")?>">картички</a></li>
          <li<?= classForItem('category7', 'current') ?>><a href="<?=aref("category.php?id=7")?>">зелено</a></li>
          <li<?= classForItem('concept', 'current') ?>><a href="<?=aref("concept.php")?>">идеята</a></li>
<?php if ($blackFriday) { ?>
          <li class="blackfriday"><a href="<?=aref("promo.php")?>"></a></li> 
<?php } ?>
	      </ul>
	      <notification id="waitNotification">
	      	<message>
	      		<p>Моля изчакайте. Заявката се обработва.</p>
	      	</message>
	      </notification>
	      <notification id="browserSupportNotification" onclick="toggleNotification('browserSupport', false)">
	      	<message>
	      		<p>Съжаляваме, но Вашият браузър е по-стара версия и не се поддържа от системата ни за поръчка.</p>
	      		<p>Можете да поръчате избраните от Вас продукти на адрес: <b>orders@creativegg.com</b></p>
	      	</message>
	      </notification>
	      <notification id="noLocalStorageNotification" onclick="toggleNotification('noLocalStorage', false)">
	      	<message>
	      		<p>Неуспешно записване в количката.</p>
		  		<p>Възможна причина е, че сте активирали Private Browsing.</p>
		  		<p>В момента този режим не се поддържа.</p>
		  	</message>
		  </notification>
