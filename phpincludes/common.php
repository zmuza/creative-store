<?php
  error_reporting(-1);
  
  /* The following function should not be called
     after any text is written to the HTML. 
     
     That includes direct HTML, empty lines and echo calls. */
  function ensure($value, $msg)
  {
    global $creativeggUrl;
    if ($value)
      return;
      
    error_log($msg, 0);
      
    header("Location: $creativeggUrl");
    die(0);
  }
  
  function aref($link)
  {
    global $creativeggUrl;
    return "$creativeggUrl/$link";
  }
  
  function productImage($file)
  {
    global $creativeggUrl;
    return "$creativeggUrl/img/products/$file";
  }
  
  $pageId = "";
  
  function classForItem($item, $className)
  {
    global $pageId;
    
    if ($pageId != $item)
      return "";
      
    return " class=\"$className\"";
  }
  
  function deliveryPrice($totalPrice, $longDist)
  {
    global $freeDeliveryLimit, $deliveryShortDist, $deliveryLongDist;

    if ($totalPrice >= $freeDeliveryLimit)
      return 0;
    
    return $longDist ? $deliveryLongDist : $deliveryShortDist;
  }
  
  function deliveryPriceText($totalPrice, $longDist)
  {
    $price = deliveryPrice($totalPrice, $longDist);

    return $price > 0 ? number_format($price, 2) . "лв." : "безплатна";
  }
  
?>
