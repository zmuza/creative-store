<?php ?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
      html, body { font-size: 10pt; }
      a:link, 
      a:visited {
        color:#59ccf4;
        text-decoration: none;
      }
      
      h2, h3 {
        margin: 0;
        font-size: 10pt;
        font-weight: bold;
      }
      
      p { margin: 12px 0 0 0; }
      
      h3 { display: inline; }
      
      h3.code { color: #59ccf4; }
      
      p.note { font-style: italic; }

      ul { 
        list-style-type: none;
        padding: 0;
        margin: 12px 0;
      }
</style>
  </head>
<body>
  <p>Този e-mail е потвърждение на Вашата поръчка.</p>
  <p><h3>Поръчка #<?= $mailInfo_orderId ?></h3></p>
  <p>Благодарим Ви, че поръчахте от <?= $creativeggLink ?>!</p>
  <p><h3>Дата:&nbsp;<h3><?= date("m.d.Y H:i", $mailInfo_orderTime) ?></p>
  <p><h3>Вашият код за плащане:&nbsp;</h3><h3 class="code"><?= $mailInfo_idn ?></h3></p>
  <p><h2>Инструкции за плащане в офис на Easypay:</h2>
    <ol>
      <li>Избор на Easypay офис.</li>
      <li>В офис на Easypay клиентът съобщава 10-цифрения код за плащане: <b><?= $mailInfo_idn ?></b></li>
    </ol>
  </p>
  <p><h2>Инструкции за плащане на банкомат:</h2>
    <ol>
      <li>Поставяне на картата в банкомата.</li>
      <li>Избиране на меню "Други услуги".</li>
      <li>Избиране на меню "B-pay".</li>
      <li>Въвеждане за код на търговеца: <b>60000</b>.</li>
      <li>Въвеждане на 10-цифрения код за плащане: <b><?= $mailInfo_idn ?></b></li>
    </ol>
  </p>
  <p><h3>Лице за контакт:&nbsp;</h3><?= $mailInfo_name ?></p>
  <p><h3>Телефон за контакт:&nbsp;</h3><?= $mailInfo_phoneNumber ?></p>
  <p><h3>Адрес за доставка:&nbsp;</h3><br /><?= $mailInfo_shippingAddress ?></p>
<? if (isset($mailInfo_billingAddress)) { ?>
  <p><h2>Адрес за плащане:</h2><?= $mailInfo_billingAddress ?></p>
<? } ?>
<? if (isset($mailInfo_invoiceInfo)) { ?>
  <p><h2>Данни за фактура:</h2> <?= $mailInfo_invoiceInfo ?></p>
<? } ?>
  <p><h2>Коментар по поръчката:</h2><?= $mailInfo_comments ?></p>
  <p>
    <h2>Продукти:</h2>
      <ul>
      <? foreach($mailInfo_orderDetails as $order) { ?>
        <li><?= $order ?></li>
      <? } ?>
      </ul>
  </p>
  <p>Междинна сума:&nbsp;<?= $mailInfo_orderValue ?><br />Доставка:&nbsp;<?= $mailInfo_shipmentPrice ?></p>
  <p><h3 style="text-transform: uppercase;">ОБЩА СУМА ЗА ПЛАЩАНЕ:&nbsp;</h3><?= $mailInfo_orderTotal ?></p>
  <p><h3>Плащане:&nbsp;</h3><?= $mailInfo_paymentTypeString ?></p>
  <p class="note">Кодът за плащане ще бъде активен в рамките на 48 часа.</p>
  <p class="note">Поръчката ще бъде изпратена само след потвърждение на плащането.</p>
</body>
</html>