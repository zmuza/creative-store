<?php ?>
    </main>
    <bottom>
	    <left>
	      <a href="<?=aref("contacts.php")?>" <?= classForItem('contacts', 'current')?>>Контакти</a>
        <a href="<?=aref("eula.php")?>" <?= classForItem('eula', 'current') ?>>Общи условия</a>
        <a href="<?=aref("shipping.php")?>" <?= classForItem('shipping', 'current') ?>>Доставка и плащане</a>
        <p>Copyright &copy;2014 Creativegg</p>
      </left>
		<right>
		  <a id="pinterestLink" href="https://www.pinterest.com/creativeggshop" target="_blank"></a>
		  <a id="facebookLink" href="https://www.facebook.com/creativeggshop" target="_blank"></a>
		</right>
	  </bottom>
    </container>
  </body>
</html>