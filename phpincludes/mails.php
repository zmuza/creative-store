<?php
$ordersEmail            = "orders@creativegg.com";
$internalEmail          = "orders@creativegg.com";
$ordersReplayToEmail    = $ordersEmail;

$paymentTypeToString    = array("", "ПРИ ДОСТАВКА", "ПРЕЗ E—PAY", "НА КАСА НА EASYPAY", "НА БАНКОМАТ");
$paymentTypeToTemplate  = array(
                            "notify" => array("", "notify.delivery", "notify.epay", "notify.easypay+bpay", "notify.easypay+bpay"),
                            "confirm" => array("", "", "confirm.epay+easypay+bpay", "confirm.epay+easypay+bpay", "confirm.epay+easypay+bpay")
                          );

function sendMail($mailInfo)
{
  global $ordersEmail, $internalEmail, $ordersReplayToEmail;
  
  $additionalHeaders = "From: $ordersEmail\r\nReplay-To: $ordersReplayToEmail\r\nBcc: $internalEmail\r\nMIME-Version: 1.0\r\nContent-type:text/html;charset=utf-8";
  
  mail($mailInfo["mail"], $mailInfo["subject"], $mailInfo["message"], $additionalHeaders);
}

function createMailWithRecipientInfo($recipientInfo, $type)
{
  global $paymentTypeToString, $paymentTypeToTemplate, $creativeggUrl, $creativeggLink;
  
  extract($recipientInfo, EXTR_PREFIX_ALL, "mailInfo");
  
  $mailInfo_paymentTypeString = $paymentTypeToString[$mailInfo_paymentType];
  
  $mailTemplate = $paymentTypeToTemplate[$type][$mailInfo_paymentType];

  ob_start();
  include("mail/$mailTemplate.php");
  $msg = ob_get_clean();
  
  $subjects = null;
  
  if ($type == 1)
    $subject = "Потвърждение на поръчка #$mailInfo_orderId";
  else
    $subject = ($type == "notify" ? "Поръчка #$mailInfo_orderId." : "Потвърждение на плащане по поръчка #$mailInfo_orderId.");
  
  return array(
    "mail"        => $mailInfo_mail,
    "subject"     => $subject,
    "message"     => $msg
  );
}
