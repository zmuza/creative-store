<?php
  require_once('phpincludes/common.php');
  require_once('phpincludes/config.php');

  $productName = isset($_GET["product"]) ? $_GET["product"] : null;
  ensure(!empty($productName) && preg_match('/^[a-zA-Z0-9_\-]+$/', $productName), "ProductName missing or incorrect: '$productName'");

  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  ensure($dbConnection, "No DB connection");
  
  $queryForId = mysqli_query($dbConnection, "SELECT product_id FROM products WHERE url_name='$productName'");
  ensure($queryForId, "Failed fetching product_id for product with name '$productName'.");
  ensure(mysqli_num_rows($queryForId) > 0, "Product with url name '$productName' not found in DB.");
  $row = mysqli_fetch_row($queryForId);
  $productId = $row[0];
  mysqli_free_result($queryForId);
  
  $queries = "SELECT products.*, (products.quantity - COALESCE(resv.quantity, 0)) as available, (promo.product_id IS NOT NULL) as promotion
              FROM products
              LEFT JOIN promo ON products.product_id=promo.product_id 
              LEFT JOIN (
                SELECT product_id, SUM( quantity ) AS quantity
                FROM reserved_products
                GROUP BY product_id
              ) AS resv ON products.product_id = resv.product_id
              WHERE products.product_id = $productId;
              
             SELECT * FROM product_images WHERE product_id = $productId ORDER BY is_primary DESC;
             
             SELECT prods.*, imgs.image FROM products as prods
             LEFT JOIN product_images as imgs
             ON prods.product_id = imgs.product_id
             WHERE imgs.is_primary=1 AND prods.product_id <> $productId
             ORDER BY RAND()
             LIMIT 4;";

  $resultSet = mysqli_multi_query($dbConnection, $queries);
  ensure($resultSet, "No DB results.");
  $productSet = mysqli_store_result($dbConnection); mysqli_next_result($dbConnection);
  ensure($productSet, "No product info.");
  $product = mysqli_fetch_array($productSet, MYSQLI_ASSOC);
  ensure($product, "No product with ID='$productId' found.");
  $imagesSet = mysqli_store_result($dbConnection); mysqli_next_result($dbConnection);
  ensure($imagesSet, "No product images.");
  $similarProductsSet = mysqli_store_result($dbConnection);
  ensure($similarProductsSet, "No similar products.");
?>
<?php 
      // Currently each product belongs to multiple categories, i.e. not possible to mark only one.
      // $categoryId = $product["category_id"];
      // $pageId = "category$categoryId";
      $additionalJS = array('product.js');
      $additionalCSS = array("product.css");
  	  $pageTitle = $product["name"];
  	  $metaDescription = $product["meta_description"];
  	  $metaKeywords = $product["meta_keywords"];
  	  
  	  if ($blackFriday && $product["promotion"])
  	    $bodyClass =  "blackfriday";
  	    
      require('phpincludes/header.php');
      
      $image = mysqli_fetch_array($imagesSet, MYSQLI_ASSOC);
?> 
  		<product_info>
  		  <img id="large_image" src="<?= productImage($image["image"]) ?>" alt="<?=$product["name"]?>" />
  		  <info>
  		    <h1><?= $product["name"] ?></h1>
			
			<fb>				
			  <p><div class="fb-like" data-href="<?=aref($product["url_name"])?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div></p>
		    </fb>
		    <twitter>
			  <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?=aref($product["url_name"])?>" data-via="creativeggshop">Tweet</a>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			</twitter>
			<googleplus>
              <!-- Place this tag where you want the +1 button to render. -->
              <div class="g-plusone" data-size="medium"></div>

              <!-- Place this tag after the last +1 button tag. -->
              <script type="text/javascript">
              window.___gcfg = {lang: 'bg'};

              (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/platform.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
               })();
              </script>			
			</googleplus>
						
			<p><?= $product["description"] ?></p>
  		    <images>
<?php
       $i = 1;
       while($image = mysqli_fetch_array($imagesSet, MYSQLI_ASSOC))
       {
?>
            <img id="small_image_<?= $i ?>" src="<?= productImage($image["image"]) ?>" alt="<?=$product["name"]?>" onclick="swapImages('large_image', 'small_image_<?= $i ?>')" />
<?php
          ++$i;
       }
?>
  		    </images>
  		    <buy>
  		      <price><?= $product["price"] ?></price>
  		      <input id="quantity_input" type="text" value="1" />
<?php if ($product["available"] > 0) { ?>
  		      <button onclick="buyProduct(<?= $product['product_id'] ?>)">купи</button>
  		    </buy>
  		    <p class="page-description hidden"><count productid="<?= $product['product_id'] ?>"></count> бр. от този продукт са във <a href="<?=aref("cart.php")?>"><u>Вашата количка</u></a></p>
<p>Можете да направите Вашата поръчка и на <b>orders@creativegg.com</b></p>

<?php } else { ?>
  		      <button disabled>купи</button>
  		    </buy>
  		    <p class="page-description">Продуктът в момента не е наличен.<br> Очакваме доставка.</p>
<?php } ?>
  		  </info>
  		</product_info>
  		<p class="page-description">Вижте още ...</p>
  		<products>
<?php
       while($row = mysqli_fetch_array($similarProductsSet, MYSQLI_ASSOC))
       {
?>
  		  <a href="<?=aref("$row[url_name]")?>">
  		    <product>
  		      <img src="<?= productImage($row["image"]) ?>" />
  		      <p><?= $row["name"] ?></p>
  		      <p><?= $row["price"] ?>лв</p>
  		    </product>
  		  </a>
<?php
	     }
?>
  		</products>
<?php 
    
    require('phpincludes/bottom.php');

    mysqli_close($dbConnection);
?>