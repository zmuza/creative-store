<?php
  require_once(realpath(__DIR__.'/..').'/phpincludes/config.php');
  
    $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  
  if (!$dbConnection)
    die(0);
    
    
  $expired = mysqli_multi_query($dbConnection, 
      "CALL sp_clear_expired_reservations;
       
       CALL  sp_clear_expired_purchases;
      ");
  
  mysqli_close($dbConnection);
  
  echo "OK";
?>