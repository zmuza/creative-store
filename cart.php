<?php
      require_once('phpincludes/common.php');
      require_once('phpincludes/config.php');

      $pageId = 'cart';
?>

<!-- BEGIN PAGE -->

<?php 
      $additionalJS  = array("cart_table/item.js", "cart_table/totals.js", "cart_table/ui.js", "cart_table/main.js");
      $additionalCSS = array("cart.css");
	    $pageTitle = "Creative Egg - Вашата количка";
      require('phpincludes/header.php')
?>
      <h1>Вашата количка</h1>
      <table id="products" cellpadding="0" cellspacing="0">
        <thead>
         <tr>
           <th colspan="2">продукт</th>
           <th>количество</th>
           <th>единична цена</th>
           <th>обща цена</th>
           <th></th>
         </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
          <tr>
            <td colspan="4">Доставка&nbsp;
              <select id="shippingPriceInput" disabled="true">
                <option value="<?= $deliveryShortDist ?>">до адрес в София</option>
                <option value="<?= $deliveryLongDist ?>">до адрес извън София</option>
              </select>
            </td>
            <td id="shippingPrice" threshold="<?= $freeDeliveryLimit ?>"><?= $deliveryShortDist ?></td>
            <td></td>
          </tr>
        </tfoot>
      </table>
      <p class="page-description grayed">За поръчки над 50лв., доставката е <b>безплатна.</b></p></p>
      <p class="page-description grayed">Можете да направите Вашата поръчка и на <b>orders@creativegg.com</b></p></p>
	  <p id="quantity_notice" class="hidden">* Няма необходимата наличност от тези продукти.</p>
      <total_price><!--<price>53,00</price>--><button id="continue" disabled="disabled">Продължи</button></total_price>
<?php

  require('phpincludes/bottom.php');
?>