-- RESERVATIONS
DROP TABLE IF EXISTS reservations;
CREATE TABLE reservations (
 reservation_id VARCHAR(64) PRIMARY KEY,
 creation       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 purchase_id     INT(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS reserved_products;
CREATE TABLE reserved_products (
 reservation_id VARCHAR(64) NOT NULL,
 product_id     INT(11) UNSIGNED NOT NULL,
 quantity       INT(11) NOT NULL,
 PRIMARY KEY (reservation_id, product_id),
 CONSTRAINT fk_reserved_products_to_reservations FOREIGN KEY (reservation_id)
  REFERENCES reservations(reservation_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
 CONSTRAINT fk_reserved_product_to_products FOREIGN KEY (product_id)
   REFERENCES products(product_id)
   ON UPDATE RESTRICT
   ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP FUNCTION IF EXISTS sf_create_new_reservation$$

CREATE FUNCTION sf_create_new_reservation() RETURNS VARCHAR(32)
BEGIN
  DECLARE rid VARCHAR(32) DEFAULT MD5(UTC_TIMESTAMP() + RAND());
  INSERT INTO reservations (reservation_id)
  VALUES (rid);

  RETURN rid;
END$$



DROP FUNCTION IF EXISTS sf_reservation_confirm$$

CREATE FUNCTION sf_reservation_confirm(reservationId VARCHAR(32))
RETURNS VARCHAR(64)
BEGIN
  DECLARE tm TIMESTAMP;
  DECLARE unavailableItems INT(11);

  SELECT creation INTO tm FROM reservations WHERE reservation_id = reservationId;

  SELECT COUNT(*) INTO unavailableItems
  FROM products as prods
  RIGHT JOIN (
      SELECT product_id, SUM( quantity ) AS quantity
      FROM reserved_products
      WHERE reservation_id IN (SELECT reservation_id FROM reservations WHERE creation <= tm)
      GROUP BY product_id
  ) AS resv ON prods.product_id = resv.product_id
  WHERE 
   (COALESCE(prods.quantity, 0) - COALESCE(resv.quantity, 0)) < 0;
      
  IF unavailableItems = 0 THEN
    RETURN reservationId;
  END IF;
  
  DELETE FROM reservations WHERE reservation_id = reservationId;

  RETURN NULL;
END$$

--- OLD FUNCTIONS


DROP PROCEDURE IF EXISTS sp_clear_expired_reservations$$
CREATE PROCEDURE sp_clear_expired_reservations()

BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS delete_reservations AS 
    (SELECT reservation_id, purchase_id FROM reservations
     WHERE TIMESTAMPDIFF(MINUTE, creation, CURRENT_TIMESTAMP()) > 15 AND
     (purchase_id is NULL or EXISTS (SELECT * FROM purchases WHERE purchases.purchase_id=reservations.purchase_id AND status=0))
    );


  DELETE FROM reservations 
  WHERE EXISTS (SELECT reservation_id FROM delete_reservations WHERE reservation_id=reservations.reservation_id);

  DELETE FROM purchases 
  WHERE EXISTS (SELECT purchase_id FROM delete_reservations WHERE purchase_id=purchases.purchase_id);
  
  DROP TABLE delete_reservations;

END$$



DROP PROCEDURE IF EXISTS sp_clear_expired_purchases$$
CREATE PROCEDURE sp_clear_expired_purchases()

BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS delete_purchases AS 
    (SELECT reservations.reservation_id as reservation_id FROM purchases
     LEFT JOIN reservations ON reservations.purchase_id = purchases.purchase_id
     WHERE
        purchases.status > 0 AND purchases.status < 253 AND
        ((purchases.payment_type = 2 AND TIMESTAMPDIFF(MINUTE, purchases.creation_time, CURRENT_TIMESTAMP()) > 16)
        OR (purchases.payment_type IN (3, 4) AND TIMESTAMPDIFF(MINUTE, purchases.creation_time, CURRENT_TIMESTAMP()) > 2900 /* 48h10m*/))
    );
    
  block_cursor: BEGIN
    DECLARE tmp INT(11); 
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE rid VARCHAR(32);
    DECLARE cur CURSOR FOR SELECT reservation_id FROM delete_purchases;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;
    
    OPEN cur;
  
    deleteLoop: LOOP
      FETCH cur INTO rid;
      IF done THEN
        LEAVE deleteLoop;
      END IF;
      SELECT sf_complete_purchase(rid, 254) INTO tmp;
    END LOOP deleteLoop;
  
    CLOSE cur;
  END block_cursor;
  
  DROP TABLE delete_purchases;

END$$


DELIMITER ;


DROP PROCEDURE IF EXISTS sp_clear_reservation$$
CREATE PROCEDURE sp_clear_reservation(IN reservationId VARCHAR(32))
BEGIN
  DECLARE purchaseId INT(11) DEFAULT NULL;
  SELECT purchase_id INTO purchaseId FROM reservations WHERE reservation_id = reservationId;
  
  DELETE FROM reservations WHERE reservation_id = reservationId;
  
  IF purchaseId IS NOT NULL THEN
    DELETE FROM purchases WHERE purchase_id = purchaseId;
  END IF;

END$$



DROP FUNCTION IF EXISTS sf_get_reservation_validity$$
CREATE FUNCTION sf_get_reservation_validity(reservationId VARCHAR(32))
RETURNS INT(11)
BEGIN
  DECLARE validFor INT(11) DEFAULT -1;
  SELECT TIMESTAMPDIFF(SECOND, CURRENT_TIMESTAMP(), TIMESTAMPADD(MINUTE, 15, lastchange)) INTO validFor
  FROM reservations
  WHERE reservation_id = reservationId;
  
  RETURN GREATEST(validFor, -1);
END$$

DELIMITER $$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_get_purchase_for_reservation$$
CREATE PROCEDURE sp_get_purchase_for_reservation(IN reservationId VARCHAR(32))
BEGIN
  DECLARE countLines INT(11) DEFAULT 0;
  DECLARE purchaseId INT(11) DEFAULT NULL;
  SELECT COUNT(*), purchase_id INTO countLines, purchaseId FROM reservations
  WHERE reservation_id = reservationId;
  
  IF countLines = 0 THEN
    SELECT NULL as purchase_id;
  ELSEIF purchaseId is NOT NULL THEN
    SELECT purchaseId as purchase_id;
  ELSE
    START TRANSACTION;
    
    INSERT INTO purchases () VALUES ();
    
    SET purchaseId = LAST_INSERT_ID();
    
    UPDATE reservations SET purchase_id = purchaseId WHERE reservation_id = reservationId;
    
    COMMIT;
    
    SELECT purchaseId as purchase_id;
  END IF;
END$$

/*CALL sp_get_purchase_for_reservation('deb51362c1888b82c8c85b8253aa321a');*/

DELIMITER ;
