-- Add primary key to product_images

ALTER TABLE product_images ADD image_id INT PRIMARY KEY AUTO_INCREMENT FIRST;

-- Add multiple categories for products --

CREATE TABLE product_category (
    product_id Int,
    category_id Int,
    PRIMARY KEY (product_id, category_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (category_id) REFERENCES categories(category_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO product_category (product_id, category_id)
SELECT product_id, category_id
FROM products;

ALTER TABLE products
DROP COLUMN category_id;

START TRANSACTION;
ALTER TABLE products ADD url_name VARCHAR(50) NOT NULL DEFAULT '' AFTER product_id;
UPDATE products SET url_name=name  WHERE url_name='';
ALTER TABLE products ADD UNIQUE (url_name);
COMMIT;

START TRANSACTION;
ALTER TABLE product_categories ADD url_name VARCHAR(50) NOT NULL DEFAULT '' AFTER category_id;
UPDATE product_categories SET url_name=name WHERE url_name='';
ALTER TABLE product_categories ADD UNIQUE (url_name);
COMMIT;

SELECT url_name, COUNT(url_name) as rep
FROM products 
GROUP BY url_name
HAVING rep > 1
ORDER BY rep DESC;


