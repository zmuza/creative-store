DELIMITER $$

DROP FUNCTION IF EXISTS sf_complete_purchase$$
CREATE FUNCTION sf_complete_purchase(reservationId VARCHAR(32), transactionStatus INT(11))
RETURNS INT(11)
BEGIN
  DECLARE purchaseId INT(11) DEFAULT NULL;
  
  SELECT purchase_id INTO purchaseId FROM reservations
  WHERE reservation_id = reservationId;
  
  IF purchaseId IS NULL THEN
    RETURN -1;
  END IF;
  
  UPDATE purchases SET status = transactionStatus
  WHERE purchase_id = purchaseId;
  
  IF transactionStatus >= 254 THEN
    INSERT INTO purchases_products (purchase_id, product_id, quantity) 
    SELECT purchaseId, product_id, quantity FROM reserved_products WHERE reservation_id = reservationId;
  END IF;
  
  IF transactionStatus = 255 THEN
    UPDATE products
      INNER JOIN 
         (SELECT rp.product_id as product_id, SUM(rp.quantity) as quantity FROM reserved_products AS rp
          WHERE rp.reservation_id=reservationId
          GROUP BY rp.product_id) as grp
        ON products.product_id = grp.product_id
      SET products.quantity = products.quantity - grp.quantity;
  END IF;
    
  DELETE FROM reservations WHERE reservation_id = reservationId;
  
  RETURN purchaseId;
END$$

DELIMITER ;