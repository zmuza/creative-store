DELIMITER $$

DROP FUNCTION IF EXISTS sf_place_order$$
CREATE FUNCTION sf_place_order(reservationId   VARCHAR(32),
                               shippingName    VARCHAR(64),
                               shippingEmail   VARCHAR(64),
                               shippingPhone   VARCHAR(32),
                               shippingAddress VARCHAR(128),
                               billingName     VARCHAR(64),
                               billingEmail    VARCHAR(64),
                               billingPhone    VARCHAR(32),
                               billingAddress  VARCHAR(128),
                               invoiceData     VARCHAR(128),
                               paymentType     INT(1), /* 1 = on delivery, 2 = epay, 3 = EasyPay, 4 = bPay */
                               comments        TEXT
                              )
RETURNS INT(11)
BEGIN
  DECLARE purchaseId INT(11) DEFAULT NULL;
  
  SELECT purchase_id INTO purchaseId FROM reservations 
  WHERE reservation_id = reservationId;
  
  IF reservationId IS NULL THEN
    RETURN NULL;
  END IF;
  
  UPDATE purchases
  SET 
     shipping_name    = shippingName,
     shipping_email   = shippingEmail,
     shipping_phone   = shippingPhone,
     shipping_address = shippingAddress,
     billing_name     = billingName,
     billing_email    = billingEmail,
     billing_phone    = billingPhone,
     billing_address  = billingAddress,
     invoice_data     = invoiceData,
     payment_type     = paymentType,
     comments         = comments,
     status           = 1
  WHERE
    purchase_id = purchaseId;
    
  RETURN purchaseId;
END$$



DELIMITER ;


/* Query to reserve items */
START TRANSACTION;

INSERT INTO purchases VALUES ();

set @pid = LAST_INSERT_ID();

INSERT INTO purchase_reserve (purchase_id, product_id, quantity)
VALUES (@pid, 6, 2),
VALUES (@pid, 41, 3);

COMMIT;



/* TRANSACTION TO MAKE A PURCHASE */
START TRANSACTION;

/* Update products' quantity */
UPDATE products 
JOIN (
    SELECT purchase_id, product_id, SUM(quantity) as quantity FROM purchase_reserve
    GROUP BY purchase_id, product_id) as purchased
ON products.product_id=purchased.product_id
SET products.quantity=products.quantity - purchased.quantity
WHERE purchased.purchase_id = 0;

/* Copy reserved items in purchased items */
INSERT INTO purchase_items 
SELECT * FROM purchase_reserve
WHERE purchase_id = 0;

/* Delete items from purchase_reserve */
DELETE FROM purchase_reserve WHERE purchase_id = 0;

/* End transaction */
COMMIT;