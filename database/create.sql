-- RECREATE TABLES --

DROP TABLE IF EXISTS product_categories;
CREATE TABLE product_categories (
 category_id    INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 name			VARCHAR(64) NOT NULL
);

DROP TABLE IF EXISTS products;
CREATE TABLE products (
 product_id     INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 category_id	INT(11) NOT NULL,
 name           VARCHAR(64) NOT NULL,
 description    BLOB NULL,
 price			DECIMAL(7,2),
 quantity       INT(11) UNSIGNED,
 CONSTRAINT fk_product_to_category FOREIGN KEY (category_id)
 REFERENCES product_categories(category_id)
 ON DELETE RESTRICT
 ON UPDATE CASCADE
);

DROP TABLE IF EXISTS product_images;
CREATE TABLE product_images (
  product_id	INT(11) UNSIGNED,
  image         VARCHAR(64),
  is_primary    TINYINT(1),
  CONSTRAINT fk_product_images_to_product FOREIGN KEY (product_id)
  REFERENCES products(product_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);


-- PURCHASES --

DROP TABLE IF EXISTS purchases;
CREATE TABLE purchases (
 purchase_id      INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 shipping_name    VARCHAR(64) NULL,
 shipping_email   VARCHAR(64) NULL,
 shipping_address VARCHAR(128) NULL,
 shipping_phone   VARCHAR(32) NULL,
 billing_name     VARCHAR(64) NULL,
 billing_email    VARCHAR(64) NULL,
 billing_address  VARCHAR(128) NULL,
 billing_phone    VARCHAR(32) NULL,
 invoice_data     TINYTEXT NULL,
 payment_type     INT(1) NOT NULL DEFAULT 0, /* 1 = on delivery, 2 = epay, 3 = EasyPay, 4 = bPay */
 comments         TEXT DEFAULT NULL,
 status           INT(1) NOT NULL DEFAULT 0, /* 0 = newly created, 1 = waiting payment, 253 = denied, 254 = expired, 255 = OK */
 purchase_key     VARCHAR(32) NULL, /* Will be used later for tracking, etc. */
 idn              VARCHAR(10) NULL,
 payment_time     DATETIME,
 stan             VARCHAR(6),
 bcode            VARCHAR(6),
 
 creation_time    TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchases_products;
CREATE TABLE purchases_products (
 purchase_id    INT(11) UNSIGNED NOT NULL, 
 product_id     INT(11) UNSIGNED NOT NULL,
 quantity       INT(11) UNSIGNED NOT NULL,
 CONSTRAINT fk_purchase_items_to_purchase FOREIGN KEY (purchase_id)
   REFERENCES purchases(purchase_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE,
 CONSTRAINT fk_purchase_items_to_product_id FOREIGN KEY (product_id)
   REFERENCES products(product_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
