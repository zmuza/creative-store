<?php
      require_once('phpincludes/config.php');
      require_once('phpincludes/common.php');
      require_once('phpincludes/recaptchalib.php');

      $pageId = 'contacts';
	    $pageTitle = "Creative Egg - Контакти";
	    
      $additionalJS  = array('contacts.js');
      require('phpincludes/header.php'); 
?>
      <style>
        #recaptcha_response_field.required { border-color: red !important; }
        #recaptcha_response_field.invalid { border-color: #FFAAAA !important; }
        
        .required:after { content: " *"; color: red; }
        .invalid:after { content: " *"; color: #FFAAAA; }
        
        main > info { display: inline-block; margin: 20px 0 80px 50px; vertical-align: top; }
        contact-form { display: block; margin: 20px 50px 0 50px; }
        main > contact-form > h1 { font-size: 14pt; font-weight: normal; text-transform: uppercase; }
        main > info > h1 { font-size: 14pt; font-weight: normal; text-transform: uppercase; }
        
        main > contact-form > p { color: #A7A9AC; margin: 12px 0 4px 0; }
        main > contact-form > p, main > contact-form > input, main > contact-form > textarea { width: 100%; }
        
        @media (min-width: 714px) {
          contact-form { display: block; margin: 20px 120px 0 50px; }
          main > contact-form > h1 { font-size: 16pt; font-weight: normal; text-transform: uppercase; }
          main > contact-form > input { width: 80%; }
        }

        @media (min-width: 1024px) {
          main > contact-form { display: inline-block; margin: 20px 0 80px 50px; vertical-align: top; }
          main > contact-form > input { }
          main > contact-form > input { width: 60%; }
          main > contact-form > textarea { width: 80%; }
          main > contact-form { width: 655px; }
        }

        main > contact-form > button {
          display: block;
          cursor: pointer;
          color: white;
          background-color: #59ccf4;
          border: 1px solid #75c4e8;
          user-select: none;
          text-transform: lowercase;
          font-size: 12pt;
          margin: 8px 0 0 0;
          padding: 4px 12px 6px 12px;
        }
          
        main > contact-form > button:disabled {
          cursor:default;
          color: #E0E0E0;
          background-color: #AEAEAE;
          border-color: #808080;
        }

        main > info > p { color: #A7A9AC; margin: 6px 0 0 0; }
        main > info > h1 { color: #6DCDF0; }
        main > info > p > company { color: black; }
      </style>
      <script type="text/javascript">
        var RecaptchaOptions = {
          theme : 'clean'
        };
      </script>      
      <contact-form>
        <h1>Изпрати запитване</h1>
        <p id="name" class="required">Име:</p><input validation="name" required autofocus tabindex="1">
        <p id="phone">Телефон:</p><input validation="phone" required tabindex="2">
        <p id="email" class="required">e-mail:</p><input validation="email" required tabindex="3">
        <p id="inquiry" class="required">Запитване</p><textarea validation="inquiry" rows="8" tabindex="4"></textarea>
        <?= recaptcha_get_html($captchaPublic, $error, true); ?>
        <button id="send">Изпрати</button>
      </contact-form>
      <info>
        <h1>Контакти</h1>
        <p>Криейтив Ег ООД</p>
        <p>ЕИК: 202743022</p>
        <p>Бул.Васил Левски №113,<br />София 1000, България</p>
        <p>Тел.: +359 894 494 544</p>
        <p>e-mail: info@creativegg.com</p><br>
    		<p>
      		<a href="https://www.facebook.com/creativeggshop" target="_blank"><img src="<?=aref("img/d/Facebook-logo.png")?>" alt="Facebook"></a> 
      		<a href="https://www.youtube.com/channel/UC0mZyHVt_SwCShKtOCBkC0Q" target="_blank"><img src="<?=aref("img/d/Youtube_logo.png")?>" alt="YouTube"></a>
      		<a href="https://www.pinterest.com/creativeggshop/" target="_blank"><img src="<?=aref("img/d/pinterest-logo.png")?>" alt="Pinterest"></a>
      		<a href="https://www.google.com/+Creativegg" target="_blank"><img src="<?=aref("img/d/GooglePlus-logo.png")?>" alt="Google+"></a>
      		<a href="https://twitter.com/creativeggshop" target="_blank"><img src="<?=aref("img/d/twitter-logo.png")?>"" alt="Twitter"></a>
    		</p>
      </info>
<?php require('phpincludes/bottom.php'); ?>