/*! Copyright 2014 Creativegg */

/* exported CartTableItem, cartTableDataUpdate */

function CartTableItem(cartItem)
{
  var signalChange = function(item)
  {
    document.ev.fire('valueChanged', { 'item' : item });
  };
    
  Object.defineProperty(this, 'productId', {
    get : function()
      {
        return cartItem.productId;
      },
    configurable : false,
    enumerable : true
  });

  Object.defineProperty(this, 'quantity', {
    get : function()
      {
        return cartItem.quantity;
      },
    set : function(value)
      {
        if (isNaN(value) || value < 0 || cartItem.quantity === value)
          return cartItem.quantity;
        
        cartItem.quantity = value;
        signalChange(this);
      },
    configurable : false,
    enumerable : true
  });
  
  var srvItem = null;

  Object.defineProperty(this, 'shouldSkip', {
    get : function()
      {
        return !srvItem;
      },
    configurable : false,
    enumerable : true
  });

  Object.defineProperty(this, 'srv', {
    get : function()
      {
        return srvItem;
      },
    set : function(value)
      {        
        srvItem = value;
        signalChange(this);
      },
    configurable : false,
    enumerable : true
  });
  
  var uiItems = { };

  Object.defineProperty(this, 'ui', {
    get : function()
      {
        return uiItems;
      },
    configurable : false,
    enumerable : true
  });
}

function cartTableDataUpdate(cartInfo, serverData)
{
  var srvData = serverData.filter(function(item) { return cartInfo.some(function(current) { return current.productId === item.productId; }); });
  
  cartInfo.forEach(function(item) {
    item.srv = null;
  });
  
  srvData.forEach(function(srvItem)
  {
    var itemInfo = cartInfo.filter(function(item) { return item.productId === srvItem.productId; })[0];
    itemInfo.srv = srvItem;
  });
  
  /* 
    All items in the cart having no information in the srv field (no info on the server)
    should be deleted. Currently, this is done by setting quantity to 0.
  */
  cartInfo.forEach(function(item) {
    if (item.shouldSkip)
      item.quantity = 0; 
  });

}
