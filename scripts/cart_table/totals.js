/*! Copyright 2014 Creativegg */

/* global createQueueTask */

/* exported CartTableTotals */

function CartTableTotals(cartInfo)
{
  var aggregateInfo = null;
  var shippingValue = 3;

  var signalChange = function(item)
  {
    document.ev.fire('totalsChanged', { 'item' : item });
  };
  
  document.ev.subscribe('valueChanged', createQueueTask(function() 
  {
    aggregateInfo = null;
    signalChange();
  }));
    
  function evaluateTotals()
  {
    aggregateInfo = cartInfo.reduce(function(aggregate, item)
      {
        if (!item.shouldSkip)
        {
          ++aggregate.items;
          aggregate.sum += item.quantity * item.srv.price;
        }
        
        return aggregate;
      }, 
      { sum : 0, items : 0 }
    );
  }
  
  function calulateShippingPrice()
  {
    var shippingPriceItem = document.getElementById('shippingPrice');
    var threshold = (shippingPriceItem && shippingPriceItem.hasAttribute('threshold')) ? parseFloat(shippingPriceItem.getAttribute('threshold')) : 100;

    return aggregateInfo.sum < threshold ? shippingValue : 0;
  }
  
  Object.defineProperty(this, 'totalPrice', {
    get : function()
      {
        if (!aggregateInfo)
          evaluateTotals();
           
        return (aggregateInfo.sum + calulateShippingPrice()).toFixed(2);
      },
    configurable : false,
    enumerable : true
  });

  Object.defineProperty(this, 'shippingPrice', {
    get : function()
      {
        if (!aggregateInfo)
          evaluateTotals();
          
        return calulateShippingPrice().toFixed(2);
      },
    configurable : false,
    enumerable : true
  });
  
  Object.defineProperty(this, 'shipping', {
  get : function()
    {
      return shippingValue;
    },
  set : function(value)
    {
      if (!isNaN(value) && shippingValue !== parseFloat(value))
      {
        shippingValue = parseFloat(value);
        signalChange();
      }
      
      return shippingValue.toFixed(2);
    },
  configurable : false,
  enumerable : true
  });
}

