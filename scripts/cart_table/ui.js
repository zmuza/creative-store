/*! Copyright 2014 Creativegg */

/* exported rowUI */

function rowUI(itemInfo)
{
  var ui = itemInfo.ui;
  
  if (ui.row)
    return ui;
    
  var productsTable = document.getElementById('products');
  
  if (!productsTable)
    return null;
  
  var tableBody = productsTable.getElementsByTagName('tbody')[0];
  
  /* CREATE ROW */
  ui.row = document.createElement('tr');
  ui.row.setAttribute('productId', itemInfo.productId);

  /* ADD PRODUCT IMAGE */
  var productImg = document.createElement('img');
  productImg.src = itemInfo.srv.image;
  var productLink = productImg.wrapIn('a');
  productLink.href = './' + itemInfo.srv.url;
  productLink.wrapIn('td').appendTo(ui.row);
  
  /* ADD PRODUCT NAME */
  productLink = document.createElement('a');
  productLink.appendChild(document.createTextNode(itemInfo.srv.name));
  productLink.href = './' + itemInfo.srv.url;
  productLink.wrapIn('td').appendTo(ui.row);
  
  /* ADD QUANTITY INPUT */
  ui.quantity = document.createElement('input');
  ui.quantity.value = itemInfo.quantity;
  var handler = (function(input, item)
  {
    return function()
    {
      item.quantity = (!input.value || isNaN(input.value)) ? 0 : Math.min(20, Math.max(0, parseInt(input.value)));
    };
  })(ui.quantity, itemInfo);
  
  ui.quantity.addEventListener('keyup', handler, false);
  ui.quantity.addEventListener('change', handler, false);
  ui.quantity.addEventListener('click', (function(input) { return function() { input.select(); }; })(ui.quantity), false);
  ui.quantity.addEventListener('blur', (function(input, item) {
      return function()
      { 
        input.value = item.quantity; 
      };
    })(ui.quantity, itemInfo), false);

  ui.quantity.wrapIn('td').appendTo(ui.row);

  /* ADD SINGLE PRICE COLUMN */
  
  ui.price = document.createElement('td');
  ui.row.appendChild(ui.price);
  
  /* ADD TOTAL PRICE COLUMN */
  // var totalItemPrice = itemInfo.srv.price * itemInfo.quantity;
  ui.totalPrice = document.createElement('td');
  ui.row.appendChild(ui.totalPrice);
  
  /* ADD COLUMN WITH BUTTON FOR ITEM REMOVAL */
  
  var rmvButton = document.createElement('button');
  rmvButton.appendChild(document.createTextNode('X'));
  rmvButton.addEventListener('click', (function()
  {
    return function() { itemInfo.quantity = 0; };
  })(itemInfo), false);
    
  rmvButton.wrapIn('td').appendTo(ui.row);
  
  tableBody.appendChild(ui.row);
  
  return ui;
}
