/*! Copyright 2014 Creativegg */

/* global alert, cart, sendServerRequest, updateCartIndicator, updateStoreData, toggleNotification, CryptoJS, navigateToPayment */

var cartInfo = [];
var aggregateInfo = null;

function updateTotals()
{
  /* SET SHIPPING PRICE */
  var shippingPrice = document.getElementById('shippingPrice');
  
  if (shippingPrice)
    shippingPrice.setContent(aggregateInfo.shippingPrice);


  /* SET TOTAL PRICE */
  var totalPriceTag = document.elementByTag('total_price');
  var tag = totalPriceTag.elementByTag('price');

  if (tag)
    tag.parentNode.removeChild(tag);
  
  if (aggregateInfo.sum === 0)
    return;
  
  var priceTag = document.createElement('price');
  priceTag.appendChild(document.createTextNode(aggregateInfo.totalPrice));
  totalPriceTag.insertBefore(priceTag, totalPriceTag.lastChild);
}

function updateQuantityNotice()
{
  var hasItems = cartInfo.length > 0 && cartInfo.some(function(item) { return item.quantity > 0; });
  var insufficientAvailability = cartInfo.some(function(item) { return item.quantity > item.srv.available; });

  var notice = document.getElementById('quantity_notice');
  if (notice)
    notice.setClass('hidden', !insufficientAvailability);
  
  var continueButton = document.getElementById('continue');
  continueButton.disabled = !hasItems || insufficientAvailability;
}

function updateCartTable()
{
  
  cartInfo.forEach(function(cartItem)
  {
    if (cartItem.shouldSkip)
      return;
      
    var ui = rowUI(cartItem);
    
    if (ui.quantity.value !== cartItem.quantity)
      ui.quantity.value = cartItem.quantity;
      
    ui.row.setClass('canceled', cartItem.quantity === 0);
    ui.row.setClass('noq', cartItem.quantity > cartItem.srv.available);
      
    ui.price.setContent(cartItem.srv.price.toFixed(2));
    ui.totalPrice.setContent((cartItem.quantity * cartItem.srv.price).toFixed(2));
  });
  
  updateQuantityNotice();
}

function requestProductsInfo()
{
  var products = cart.products.map(function(item) { return item.productId; });
  sendServerRequest('fetchProductsData', products, function(code, response)
  {
      if ( response && (response instanceof Array) )
      {
        cartTableDataUpdate(cartInfo, response);
        updateCartIndicator();
      }
  });
}

function reserveProducts()
{
  if (!cart.products || !cart.products.length)
    return;
  
  toggleNotification('wait', true);

  var requestBody = {
      products  : cart.products.filter(function(item) { return item.quantity > 0; })
  };
  
  if (cart.order)
    requestBody.orderId = cart.order.id;
  
  sendServerRequest('reserveProducts', requestBody, function(code, response)
  {
    toggleNotification('wait', false);
    
    if (code !== 200)
    {
  	  alert('Невалиден отговор върнат от сървъра.');
      return;
    }

    if ( !response )
      return;

    if (response.error === 'input')
    {
      delete cart.order;
      requestProductsInfo();
    }
    else
    {
      cart.order = {
        id    : response.orderId,
        sha256  : CryptoJS.SHA256(JSON.stringify(cart.products)).toString(CryptoJS.enc.Base64)
      };

      setTimeout(function ()
      {
        if (!navigateToPayment(false))
        {
          requestProductsInfo();
        }
      }, 0);
    }
  });
}

function fillInCartTable()
{
  cartInfo = cart.products.map(function(item) { return new CartTableItem(item); });
  aggregateInfo = new CartTableTotals(cartInfo);

  var selectInput = document.getElementById('shippingPriceInput');
  
  if (selectInput)
  {
    if (typeof cart.shortDist === "undefined")
    {
      cart.shortDist = (selectInput.selectedIndex === 0);
    }
    else
    {
      selectInput.selectedIndex = cart.shortDist ? 0 : 1;
      aggregateInfo.shipping = selectInput.value;
    }

    selectInput.addEventListener('change', (function(select)
    {
      return function()
      {
        aggregateInfo.shipping = select.value;
        cart.shortDist = (select.selectedIndex === 0);
      };
    })(selectInput), false);
    
    selectInput.disabled = false;
  }
  
  requestProductsInfo();
}

window.addEventListener('load', function()
  {
    if (!document.browserSupported)
    {
      toggleNotification('browserSupport', true);
      return;
    }
    
    var continueButton = document.getElementById('continue');
    if (continueButton)
      continueButton.addEventListener('click', reserveProducts, false);
    
    if (cart === undefined)
      document.ev.subscribe('cartLoaded', fillInCartTable);
    else
      fillInCartTable();
      
    document.ev.subscribe('valueChanged', createQueueTask(function() 
      {
        updateStoreData();
        updateCartTable();
        updateCartIndicator();
      }));
    
    document.ev.subscribe('totalsChanged', createQueueTask(function()
      {
        updateTotals();
      }));
  }, 
  false);

