/*! Copyright 2014 Creativegg */

/* exported createInputUpdate, createLengthInputValidation, createRegexInputValidation */

function createInputUpdate(className, validator, target)
{
  var invalidClass  = typeof className === "string" ? className : className.invalid;
  var requiredClass = typeof className === "string" ? className : className.required;

  return function (event)
  {
    var source = (event instanceof HTMLElement) ? event : (event.target || event.srcElement);
    
    (target || source).clearClass(invalidClass);
    (target || source).clearClass(requiredClass);

    if (!source.value || !source.value.length)
    {
      (target || source).addClass(requiredClass);
      return;
    }

    var r = validator(source);

    (target || source).setClass(invalidClass, !r);
  };
}

function createLengthInputValidation(len)
{
  return len === -1 ?
    function() { return true; } :
    function (input)
    {
      return input.value && input.value.length >= len;
    };
}

function createRegexInputValidation(regex)
{
  return function(input)
  {
    return regex.test(input.value);
  };
}
