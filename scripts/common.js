/*! Copyright 2014 Creativegg */

/* global HTMLCollection */

/* exported swapImages, createQueueTask, toggleNotification */

HTMLElement.prototype.clearClass = function(className)
{
  if (!this.className)
    return;

  this.className = this.className.replace(className, '');  
};

HTMLElement.prototype.elementByTag = function(tagName)
{
  var subTags = tagName.split('.');
  
  var item = this;
  
  for(var i = 0; i < subTags.length; i++)
  {
    var itemsFound = item.getElementsByTagName(subTags[i]);
    
    if (!itemsFound || !itemsFound.length)
      return null;
    
    item = itemsFound[0];
  }
  
  return item;
};

document.elementByTag = HTMLElement.prototype.elementByTag;

HTMLElement.prototype.addClass = function(className)
{
  if (!this.className)
  {
    this.className = className;
    return;
  } 
  
  if (this.className.match(className))
    return;
    
  this.className += ' ' + className;
};

HTMLElement.prototype.setClass = function(className, flag)
{
  if (flag)
    this.addClass(className);
  else
    this.clearClass(className);
};

HTMLElement.prototype.replaceClass = function(oldClassName, newClassName)
{
  if (this.className)
    this.className = this.className.replace(oldClassName, newClassName);
};

HTMLElement.prototype.wrapIn = function(tagName)
{
  if (!tagName)
    return this;
    
  var element = document.createElement(tagName);
  element.appendChild(this);
  
  return element;
};

HTMLElement.prototype.appendTo = function(item)
{
  if (!item)
    return this;
    
  item.appendChild(this);
  
  return item;
};

HTMLElement.prototype.setContent = function(content)
{
  while(this.lastChild)
    this.removeChild(this.lastChild);
    
  this.appendChild(document.createTextNode(content));
};

HTMLCollection.prototype.asArray = function()
{
  return Array.prototype.slice.call(this);
};

NodeList.prototype.asArray = function()
{
  return Array.prototype.slice.call(this);
};

function swapImages(imgId1, imgId2)
{
  var img1 = document.getElementById(imgId1);
  var img2 = document.getElementById(imgId2);
  
  if (!img1 || ! img2)
    return;
  
  var tmp = img1.src;
  img1.src = img2.src;
  img2.src = tmp;
}

document.ev = (function()
{
  var events = [];
  
  this.subscribe = function(eventName, callback)
  {
    /*jshint -W018 */
    if ((!eventName) || !(typeof callback === 'function'))
      return false;
    /*jshint +W018 */
    var list = events[eventName];
    
    if (!list)
      events[eventName] = list = [];
    
    list.push(callback);
    
    return true;
  };

  this.unsubscribe = function(eventName, callback)
  {
    var list = events[eventName];
    
    if (!list)
      return false;
    
    list.remove(callback);    
  };
  
  this.fire = function(eventName, params)
  {
    var list = events[eventName];
    
    if (!list)
      return false;
    
    for (var i = 0; i < list.length; i++)
    {
      setTimeout(list[i].bind(null, params), 0);
    }
    
    return list.length;
  };
  
  return this;
})();

function createQueueTask(task)
{
  if (!task || (typeof task !== 'function'))
    return null;
    
  var timeoutId = null;
  
  return function() {
    if (timeoutId)
      return;

    var callArgs = arguments;
    timeoutId = setTimeout(function()
    {
      timeoutId = null;
      task.apply(callArgs);
    }, 0);
  };
}

function toggleNotification(id, show)
{
  var notification = document.getElementById(id + 'Notification');
  
  if (!notification)
    return;
  
  notification.setClass('visible', show);
}

try {
  var N=navigator.appName, ua=navigator.userAgent, tem;
  var M=ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
  if(M && (tem= ua.match(/version\/([\.\d]+)/i)) !== null) M[2]= tem[1];
  M = M ? [M[1], M[2]]: [N, navigator.appVersion, '-?'];
  var info = { name : M[0], version : M[1] };
  navigator.browser = info; 
  
  document.browserSupported = (info.name.toUpperCase() !== 'MSIE' || parseFloat(info.version) >= 9);
}
catch(e)
{
  document.browserSupported = null;
}

document.localStorageAvailable = true;

try
{
  // try to use localStorage
  localStorage.test = 2;
  delete localStorage.test;
}
catch(e)
{
  document.localStorageAvailable = false;
}
