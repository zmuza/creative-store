/*! Copyright 2014 Creativegg */

/* global console, alert */

/* exported sendServerRequest, reportError */

function sendServerRequest(service, payload, callback)
{
  if (!callback)
    return;
  
  var request = new XMLHttpRequest();
  request.open("POST", "https://creativegg.com/svc/" + service + ".php");
  request.setRequestHeader("Content-type", "application/json", true);

  request.addEventListener('load', function() 
  {
	  if (request.readyState !== 4)
      return;
    
	  if (request.status !== 200) {
      console.log('Server communication error: ' + request.status);
	    alert('Server communication error: ' + request.status);
	    callback(request.status, null);
		  return;
	  }
	  
	  try
	  {
	    var response = request.responseText ? JSON.parse(request.responseText) : null;
      
      if ( response )
        callback(200, response);
	  } 
	  catch(err) {
  	  console.log(err + ':: ' + response);
  	  callback(-1, null);
	  }
  }, 
  false);
  
  try
  {
    request.send(JSON.stringify(payload));
  }
  catch(e)
  {
    
  }
}

function reportError(message, filename, lineno)
{
  setTimeout((function(page, errorMessage, url, lineNumber)
  {
    return function()
    {
      var data = {
        applicationName : navigator.appName,
        userAgent       : navigator.userAgent,
        errorMessage    : errorMessage,
        page            : page,
        url             : url,
        lineNumber      : lineNumber
      };
      
      sendServerRequest('reportJavascriptError', data, function(/*errorCode, data*/) { });
    };
  })(window.location ? (window.location.href || window.location.origin) : 'error', message, filename, lineno), 0);
}

if (navigator.userAgent.search('Firefox') !== -1)
{
  window.onerror = function(message, filename, lineno)
  {
    reportError(message, filename, lineno);
    return true;
  };
}
else
{
  window.addEventListener('error', function(error)
  {
    reportError(error.message || 'undefined', error.filename || 'undefined', error.lineno || 'undefined');
    
    return false;
  });
}