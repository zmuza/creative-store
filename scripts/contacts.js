/*! Copyright 2014 Creativegg */

function inputFields()
{
  return document.querySelectorAll('[validation]').asArray();
}

function fetchInputData()
{
  var res = { };
  
  
  var r = inputFields().every(function(item, idx) {
    
    if (item.validator && !item.validator(item))
    {
      item.focus();
      return false;
    }
    
    if (item.value)
    {
      var key = item.getAttribute('validation');
      res[key] = item.value;
    }
    
    return true;
  });
  
  return r ? res : null;
}

function validateForm()
{
  inputFields().forEach(function(item, idx) {
    if (item.changeHandler)
      item.changeHandler(item);
  });
}

function submitInquiry(ev)
{
  var data = fetchInputData();
  if (!data)
    return;
  
  data.challenge = document.getElementById('recaptcha_challenge_field').value;
  
  sendServerRequest('sendInquiry', data, function(code, response)
  {
    if (code === 200 && response && response.status === 'OK')
    {
      window.location = "/complete.php?p=inquiry";
      return;    
    }
    
    if (response.error == "missing" || response.error == "missing")
    {
      var missing = inputFields().filter(function(item) { return response.fields.indexOf(item.validation) !== -1; });
      
      missing.forEach(function(inputField)
      {
        inputField.setClass('invalid');
      });
      
      missing[0].focus();
    }
    else if (response.error == "captcha")
    {
      var captcha = document.getElementById('recaptcha_response_field');

      if (captcha.select)
        captcha.select();

      captcha.addClass('invalid');
    }
    else
    {
      var msg = 'Възникна грешка при изпращане на съобщението' + (response ? (': "' + response.message + '"') : '.');
      console.log('Код ' + code + '; ' + msg);
    }
    
    alert('Във формата има невалидни данни. Моля, коригирайте ги.');
  });
}

window.addEventListener('load', function() {

  var validationRegex = {
    'name'     : /^\s*\S.{1,30}\S\s*$/,
    'email'    : /[^@]+@.+\..+/,
    'inquiry'  : /^\s*\S.{8,1998}\S\s*$/m,
    'response' : /^.+$/
  };

  var captcha = document.getElementById('recaptcha_response_field');
  if (captcha)
    captcha.setAttribute('validation', 'response');

  
  var sendButton = document.getElementById('send');
  if (sendButton)
    sendButton.addEventListener('click', submitInquiry, false);
  
  inputFields().forEach(function(inputField, index)
  {
    var key = inputField.getAttribute('validation');
    var regex = validationRegex[key];
    
    if (!regex)
      return;
      
    var target = document.getElementById(key);
    
    inputField.validator = createRegexInputValidation(validationRegex[key]);
    inputField.changeHandler = createInputUpdate({invalid:'invalid', required:'required'}, inputField.validator, target);
    
    inputField.addEventListener('change', inputField.changeHandler, false);
    inputField.addEventListener('keyup', inputField.changeHandler, false);
  });
  
  validateForm();
  
}, false);