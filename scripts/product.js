/*! Copyright 2014 Creativegg */

/* global alert, cart, toggleNotification, updateCartIndicator, updateStoreData, swapImages */

/* exported updateProductCount, buyProduct */

function updateProductCount()
{
  var allCountTags = document.getElementsByTagName('count');
  var countTag = allCountTags && allCountTags.length > 0 ? allCountTags[0] : null;
  
  if (!countTag)
    return;
    
  try {
    var productId = parseInt(countTag.getAttribute('productid'));
    
    var products = cart.products.filter(function(item) { return item.productId === productId; });
    
    if (!products || !products.length || !products[0].quantity)
      return;
    
    countTag.innerHTML = '';
    countTag.appendChild(document.createTextNode(products[0].quantity));
    countTag.parentNode.clearClass('hidden');
  }
  catch(err) {
  }
}

function buyProduct(productId)
{
  if (!document.browserSupported)
  {
    toggleNotification('browserSupport', true);
    return;
  }
  
  if (!document.localStorageAvailable)
  {
    toggleNotification('noLocalStorage', true);
    return;
  }
  
  var quantityInput = document.getElementById('quantity_input');
  var quantity = isNaN(quantityInput.value) ? 0 : parseInt(quantityInput.value);
  if (quantity < 1 || quantity > 20)
  {
    alert('Количеството трябва да е число от 1 до 20');
    quantityInput.focus();
    quantityInput.select();
    return;
  }
  
  var l = cart.products.filter(function(cur) { return cur.productId === productId; });
  
  if (l.length > 0)
    l[0].quantity = Math.min(20, l[0].quantity + quantity);
  else
    cart.products.push({ productId: productId, quantity: quantity });

  updateCartIndicator();
  updateProductCount();
  updateStoreData();
  
  window.location = "/cart.php";
}

window.addEventListener('load', function()
{
  var mainImg = document.getElementById('large_image');
  var images = document.querySelectorAll('img').asArray().map(function(item) { return item.id; }).filter(function(id)
  {
    return /small_image_\d{1,3}/.test(id);
  });
  
  var touchStartPosition = { x: NaN, y: NaN };

  mainImg.addEventListener("touchstart", (function(startPos)
  {
    return function(e)
    {
      if (e.changedTouches.length !== 1)
      {
        startPos.x = startPos.y = NaN;  
        return;
      }
      
      var tObj = e.changedTouches[0];
      startPos.x = parseInt(tObj.clientX);
      startPos.y = parseInt(tObj.clientY);
    };
  })(touchStartPosition), false);
  
  mainImg.addEventListener("touchend", (function(startPos, images)
  {
    return function(e)
    {
      if (isNaN(startPos.x) || isNaN(startPos.y))
        return;
        
      var pos = { x: parseInt(e.changedTouches[0].clientX), y: parseInt(e.changedTouches[0].clientY) };
        
      if (Math.abs(startPos.x - pos.x) > 10 || Math.abs(startPos.y - pos.y)  > 10)
        return;
        
      var current = images.shift();
      images.push(current);
      swapImages('large_image', current);
    };
  })(touchStartPosition, images), false);

  document.ev.subscribe('cartLoaded', function(e) { updateProductCount(e.cart);}, false);
  if (cart) {
    updateProductCount(cart);
  }
}, false);