/*! Copyright 2014 Creativegg */

window.addEventListener('load', function() {
  var slideshow = document.getElementsByTagName('slideshow')[0];
  var slideshowDivs = slideshow.getElementsByTagName('div');
  var items = slideshowDivs.asArray();
  
  items.forEach(function(element) {
    element.addEventListener('click', function()
    {
      var url = this.getAttribute('page');
      
      if (url)
        window.location = "./" + url;

    }.bind(element), false);
  });
  
  (function(items)
  {
    if (!items)
      return null;
    
    var current = 0;
    
    document.slideshowIntervalId = window.setInterval(function()
    {
      var prev = current;
      current = (current + 1) % items.length;
      items[prev].replaceClass('show', 'hide');
      items[current].replaceClass('hide', 'show');
    }, 3000);
  })(items);
  
}, false);
