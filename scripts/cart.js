var cart;

function updateCartIndicator()
{
  var cartItems = document.getElementsByTagName('cart_items');
  
  if (!cartItems || !cartItems.length)
    return;
  
  var sum = cart.products.reduce(function(previous, item)
  {
    return item.quantity > 0 ? previous + 1 : previous;
    
  }, 0);
  
  cartItems[0].setContent(sum);
}

function navigateToPayment(navigateToCartOnFail)
{
  var nvg = navigateToCartOnFail ? function() { window.location = "./cart.php"; } : function () { };
  
  if (!cart.order)
  {
    nvg();
    return false;
  }
    
  var sha256 = CryptoJS.SHA256(JSON.stringify(cart.products)).toString(CryptoJS.enc.Base64);
  
  if (sha256 != cart.order.sha256)
  {
    nvg();    
    return false;
  }

  window.location = "./payment.php?id=" + cart.order.id;
  
  return true;
}

window.addEventListener('load', function()
  {
    try
    {
      if (localStorage && localStorage.cart) cart = JSON.parse(localStorage.cart);
      else if (sessionStorage && sessionStorage.cart) cart = JSON.parse(sessionStorage.cart);
      else cart = { products: [] };
    } 
    catch(err)
    {
    }
    
    if (!cart)
      cart = { products: [] };

    updateCartIndicator();
    
    if (cart.order)
    {
      var paymentLink = document.getElementById('paymentLink');
      if (paymentLink)
        paymentLink.href += "?id=" + cart.order.id;
    }
    
    document.ev.fire('cartLoaded', { 'cart' : cart });
  }, 
  false);

function updateStoreData()
{
  if (!cart)
    return;
  
  if (!cart.products)
    cart.products = [];

  cart.products = cart.products.filter(function(item)
  {
    return item.quantity > 0;
  });

  if (localStorage)
    localStorage.cart = JSON.stringify(cart);

  if (sessionStorage)
    sessionStorage.cart = JSON.stringify(cart);
}

function emptyCart()
{
  cart = { products: [] };
  
  updateStoreData();
}

function storeOnUnload()
{
  updateStoreData();
  window.removeEventListener('beforeunload', storeOnUnload, false);
  window.removeEventListener('unload', updateStoreData, false);
}

window.addEventListener('beforeunload', storeOnUnload, false);
window.addEventListener('unload', updateStoreData, false);
