/*! Copyright 2014 Creativegg */

/* global console, createInputUpdate, createLengthInputValidation, createRegexInputValidation */

function createCheckboxAction(checkboxId, targetId)
{
  var checkbox = document.getElementById(checkboxId);
  if (!checkbox)
    return function () { };
  
  var target = document.getElementById(targetId);
  if (!target)
    return function () { };
    
  return function()
  {
    target.setClass('hidden', !checkbox.checked);
  };
}

function deliveryMunicipalityChanged()
{
  var deliveryPrice = document.getElementById('deliveryPrice');
  var totalPrice = document.getElementById('totalPrice');
  var source = document.getElementById('deliveryMunicipality');

  var deliveryPriceValue = parseFloat(deliveryPrice.getAttribute('long'));
  if (/^\s*(((гр|gr)\.?)|grad\s|град\s)?\s*(sof(i|y|iy)(a|q)|софия)(\s+(grad|град))?\s*$/i.test(source.value))
    deliveryPriceValue = parseFloat(deliveryPrice.getAttribute('short'));
    
  var totalPriceValue = parseFloat(totalPrice.getAttribute('price')) + deliveryPriceValue;
  
  deliveryPrice.setContent(deliveryPriceValue.toFixed(2) + 'лв.');
  totalPrice.setContent(totalPriceValue.toFixed(2));
}

function initInputs()
{
  var dict = {
/*    "recipientName"     : /([\u0400-\u04FF]{1,2}(\.|[\u0400-\u04FF]+\s)\s*[\u0400-\u04FF]{1,2}(\.(\s*[\u0400-\u04FF]+)|[\u0400-\u04FF]+(\s+[\u0400-\u04FF]+)?))|(\w{1,2}(\.|\w+\s)\s*\w{1,2}(\.(\s*\w+)|\w+(\s+\w+)?))/,*/
    "recipientName"     : 3,
    "municipality"      : 1,
    "address"           : 1,
    "postCode"          : -1,
    "phoneNumber"       : /^\s*((\+|00)359)?(\s|\d)+\s*$/,
    "email"             : /[^@]+@.+\..+/,
    "companyName"       : 3,
    "unifiedIdNumber"   : /^\s*(BG)?\d{8,9}\s*$/,
    "mol"               : -1
  };
  
  var validators = { };
  
  var el = document.querySelectorAll('input[validation]');
    
  if (!el.length)
    return;
  
  el.asArray().forEach(function(item) {
    var key = item.getAttribute('validation');
    
    if (validators[key])
    {
      item.validator = validators[key].validator;
      item.addEventListener('change', validators[key].handler, false);
      item.addEventListener('keyup', validators[key].handler, false);
      return;
    }
    
    if (!dict[key])
    {
      console.log("Validator info for input type '" + key + "' not found.");
      return;
    }
    
    var v = { };
    
    if (typeof dict[key] === 'number')
      v.validator = createLengthInputValidation(dict[key]);
    else
      v.validator = createRegexInputValidation(dict[key]);
      
    v.handler = createInputUpdate('invalid', v.validator);
    
    validators[key] = v;
    item.validator = v.validator;
    item.addEventListener('change', v.handler, false);
    item.addEventListener('keyup', v.handler, false);
  });
}

window.addEventListener('load', function() {
  var invoiceCheck = document.getElementById('requireInvoiceCheckbox');
  
  invoiceCheck.addEventListener('change', createCheckboxAction('requireInvoiceCheckbox', 'invoice_info'), false);
  
  var deliveryPrice = document.getElementById('deliveryPrice');

  if (deliveryPrice) // The price is below the free shipping limit
  { 
    var input = document.getElementById('deliveryMunicipality');
    input.addEventListener('change', deliveryMunicipalityChanged, false); 
    input.addEventListener('keyup', deliveryMunicipalityChanged, false); 
  }
  
  initInputs();
  
}, false);
