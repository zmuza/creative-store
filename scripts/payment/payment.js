/*! Copyright 2014 Creativegg */

/* global alert, cart, toggleNotification, sendServerRequest, emptyCart */

function fetchInputData(contact)
{
  var res = { };
  
  var inputElements = contact.getElementsByTagName('input');
  
  var arr = inputElements.asArray();
  
  for (var i = 0; i < arr.length; i++)
  {
    var item = arr[i];
    
    if (!item.validator(item))
    {
      item.focus();
      return null;
    }
    
    var key = item.getAttribute('validation');
    
    res[key] = item.value;
  }
  
  return res;
}

function getRadioChoice()
{
  var radio = document.getElementsByName('paymentType').asArray();
  
  if (!radio.length)
    return null;
  
  for (var i = 0; i < radio.length; i++)
    if (radio[i].checked)
    {
      document.elementByTag('payment_options').clearClass('invalid');
      return parseInt(radio[i].getAttribute('returns'));
    }
    
  document.elementByTag('payment_options').addClass('invalid');
    
  radio[0].focus();
  
  return null;
}

function prepareOrder()
{
  var order = { };
  
  order.orderId = cart.order.id;
  
  order.shipping = fetchInputData(document.getElementById('delivery_address'));
  if (order.shipping === null)
    return null;
     
  if (document.getElementById('requireInvoiceCheckbox').checked)
  {
    order.invoice = fetchInputData(document.getElementById('invoice_info'));
    if (order.invoice === null)
      return null;
  }
  
  order.paymentType = getRadioChoice();
  if (!order.paymentType)
    return null;
  
  order.comments = document.getElementById('orderComments').value;
  
  return order;
}

function placeOrder()
{
  var order = prepareOrder();
  
  if (!order)
    return;
  
  toggleNotification('wait', true);
  
  sendServerRequest('placeOrder', order, function(code, response)
  {
    toggleNotification('wait', false);
    
    if (code !== 200 || !response || response.status !== 'OK')
    {
      console.log('Грешка ' + code + ': ' + (response ? response.message : 'NULL'));
      alert('Възникна грешка.');
      return;      
    }
    
    emptyCart();
    
    switch(response.nav)
    {
      case 'ePay':
        var url = document.getElementById('order').getAttribute('purl');
        postToUrl(url, response.data);
        return;
      default:
        window.location = "./complete.php?p="+ response.nav;
        return;
    }
  });
}

window.addEventListener('load', function() {
    var orderButton = document.getElementById('order');
    if (orderButton)
      orderButton.addEventListener('click', placeOrder, false);
      
    if (cart.shortDist)
    {
      
      document.getElementById('deliveryMunicipality').value = "София";
      deliveryMunicipalityChanged();
    }
      
    var radio = document.getElementsByName('paymentType').asArray();
    
    radio.forEach(function(radioButton) {
      radioButton.addEventListener('change', getRadioChoice, false);
    });
  
}, false);
