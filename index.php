<?php
      require_once('phpincludes/config.php');
      require_once('phpincludes/common.php');
      
      $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
      ensure($dbConnection, "No DB connection");
      
      $products = mysqli_query($dbConnection,
        "SELECT prods.*, imgs.image FROM products as prods
         LEFT JOIN product_images as imgs
         ON prods.product_id = imgs.product_id
         WHERE imgs.is_primary=1
         ORDER BY RAND()
         LIMIT 6;"
      );
      
      ensure($products, "Error fetching products data.");
?>
<?php 
      $additionalJS = array("main.js");
      $additionalCSS = array("main.css");
      $pageTitle = "Creative Egg - интересни, уникални и креативни подаръци.";
      $titleTrailing = "";
      require('phpincludes/header.php')
?> 
  		<slideshow>
  		  <div page="kartasviat" class="show"></div>
  		  <div page="sluncezakuska" class="hide"></div>
  		  <div page="opashchici" class="hide"></div>
  		</slideshow>
		
		<about>
		<div id="about">
		<p>Тук ще намерите качествени подаръци, които изненадват, провокират, разсмиват ...</p>
		</div>
		<!--
		<div id="about">
			<p>Creativegg.com е мястото за креативни, оригинални и нестандартни подаръци. 
			Тук ще намерите качествени подаръци, които изненадват, провокират, разсмиват 
			и най-вече внасят повече усмивки в ежедневието. Като например култовата <a href="<?=aref("product.php?id=31")?>">Скреч карта Свят</a>.
                        При нас можете да откриете също така интересни идеи <a href="<?=aref("category.php?id=1")?>">за дома</a>, 
			<a href="<?=aref("category.php?id=2")?>">за кухнята</a>, 
			<a href="<?=aref("category.php?id=3")?>">за офиса</a>, 
			както и <a href="<?=aref("category.php?id=4")?>">за по-креативно настроените</a>. 
			За нас е важно, да Ви предлагаме качествени подаръци, които освен че са умни, 
			красиви и практични, са произведени от рециклируеми материали с мисъл за природата.</p>
			<p>Всяка седмица, очаквайте различни култови продукти на <a href="<?=aref("promo.php")?>">промоционални цени.</a></p>
		</div>
		-->
		</about>
		
  		<products>
<?php
       while($row = mysqli_fetch_array($products, MYSQLI_ASSOC))
       {
?>
  		  <a href="<?=aref($row["url_name"])?>">
  		    <product>
  		      <img src="<?= productImage($row["image"]) ?>" alt="<?= $row["name"] ?>" />
  		      <h1><?= $row["name"] ?></h1>
  		      <p><?= $row["price"] ?> лв</p>
  		    </product>
  		  </a>
<?php
	     }
?>
  		</products>
<?php

  require('phpincludes/bottom.php');
  
  mysqli_close($dbConnection);  
?>