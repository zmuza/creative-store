<?php
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (!array_key_exists("shipping", $data) 
      || !array_key_exists("paymentType", $data)
      || $data["paymentType"] < 1 
      || $data["paymentType"] > 4
      || !array_key_exists("orderId", $data)
      || !preg_match('/^[0-9a-zA-Z]*$/', $data["orderId"]))
  {
    echo json_encode(array("error" => "input", "message" => "Подадени са невалидни данни."));
    die(0);
  }

  $shipping = $data["shipping"];
  
  if (!array_key_exists("municipality", $shipping) || !array_key_exists("address", $shipping))
  {
    echo json_encode(array("error" => "input", "message" => "Подадени са непълни данни."));
    die(0);
  }
  
  
  $reservationId = $data["orderId"];
  
  require_once('../phpincludes/config.php'); 
  require_once('../phpincludes/common.php');

  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);

  if (!$dbConnection)
  {
    echo json_encode(array("error" => "database", "message" => "Проблем при връзката с базата данни."));
    die(0);
  }
  
  /* EVALUATE SHIPPING AND BILLING ADDRESSES */
  
  $postCode = array_key_exists("postCode", $shipping) ? (" " . $shipping["postCode"]) : "";
  
  $shippingAddress = mysqli_real_escape_string($dbConnection, $shipping["municipality"] . $postCode . ", " . $shipping["address"]);
  
  $billingAddress = null;
  if (array_key_exists("billing", $data))
  {
    $billing = $data["billing"];
    $billingAddress = mysqli_real_escape_string($dbConnection, $billing["municipality"] . " " . $billing["postCode"] . ", " . $billing["address"]);
  }
  
  $invoiceData = null;
  
  if (array_key_exists("invoice", $data))
  {
    $ivc = $data["invoice"];
    $invoiceData = $ivc["companyName"]."\n"
                   .$ivc["municipality"].", ".$ivc["address"].".\n"
                   ."ЕИК/ЕИК по ДДС:".$ivc["unifiedIdNumber"];
    if ($ivc["mol"])
      $invoiceData .= "\nМОЛ:".$ivc["mol"];
      
    unset($ivc);
  }

  $paymentType = $data["paymentType"];
  
  /* UPDATE THE RECORD IN THE PURCHASES TABLE */
  
  $placeOrderQuery = "SELECT sf_place_order(" . implode(", ", array(
    "'".mysqli_real_escape_string($dbConnection, $reservationId)."'",
    "'".mysqli_real_escape_string($dbConnection, $shipping["recipientName"])."'",
    "'".mysqli_real_escape_string($dbConnection, $shipping["email"])."'",
    "'".mysqli_real_escape_string($dbConnection, $shipping["phoneNumber"])."'",
    "'".mysqli_real_escape_string($dbConnection, $shippingAddress)."'",
    isset($billing) ? "'".mysqli_real_escape_string($dbConnection, $billing["recipientName"])."'" : "NULL",
    isset($billing) ? "'".mysqli_real_escape_string($dbConnection, $billing["email"])."'" : "NULL",
    isset($billing) ? "'".mysqli_real_escape_string($dbConnection, $billing["phoneNumber"])."'" : "NULL",
    isset($billingAddress) ? "'".mysqli_real_escape_string($dbConnection, $billingAddress)."'" : "NULL",
    isset($invoiceData) ? "'".mysqli_real_escape_string($dbConnection, $invoiceData)."'" : "NULL",
    $paymentType,
    "'".mysqli_real_escape_string($dbConnection, $data["comments"])."'"
  )).") INTO @orderNumber;";
  
  $results = mysqli_multi_query($dbConnection, "set @orderNumber = 0;
      $placeOrderQuery
      SELECT reserved_products.*, products.name, products.price, (reserved_products.quantity * products.price) as total
      FROM reserved_products
      LEFT JOIN products ON reserved_products.product_id=products.product_id
      WHERE reserved_products.reservation_id='$reservationId';
      SELECT @orderNumber as order_number;"
  );
  
  /* FETCH QUERY RESULTS: ORDER_ID, PRODUCTS INFORMATION FOR RESERVATION */

    
  mysqli_next_result($dbConnection); mysqli_next_result($dbConnection);
  
  $productsResult = mysqli_store_result($dbConnection); mysqli_next_result($dbConnection);

  if (!mysqli_num_rows($productsResult))
  {
    echo json_encode(array("error" => "input", "message" => "Не са намерени продукти към поръчката."));
    die(0);
  }
  
  $placeOrder = mysqli_store_result($dbConnection);
  
  $row = mysqli_fetch_array($placeOrder, MYSQLI_NUM);
  $orderNumber = $row[0];

  $orderDetails = array();
  $total = 0;
  
  while ($row = mysqli_fetch_array($productsResult, MYSQLI_ASSOC))
  {
    extract($row, EXTR_PREFIX_ALL, "product");
    $total += $product_quantity * $product_price;
    array_push($orderDetails, "$product_quantity x $product_name за " . number_format($product_price, 2) . "лв за брой");
  }
  
  mysqli_free_result($productsResult);
  
  while(mysqli_more_results($dbConnection))
  {
    mysqli_next_result($dbConnection);
    $r = mysqli_store_result($dbConnection);
    if (!is_bool($r))
      mysqli_free_result($r);
  }
  
  /* EVALUATE SHIPPING PRICE */
  
  $shippingPrice = 0;
  
  if ($total < $freeDeliveryLimit)
  {
    if (preg_match("/^\\s*(((гр|gr)\\.?)|grad\\s|град\\s)?\\s*(sof(i|y|iy)(a|q)|софия)(\\s+(grad|град))?\\s*$/iu", $shipping["municipality"]))
      $shippingPrice = $deliveryShortDist;
    else
      $shippingPrice = $deliveryLongDist;
  }
  
  $orderTotal = $total + $shippingPrice;
    
  $comments = array_key_exists("comments", $data) ? $data["comments"] : "-";
  
  /* REQUEST EASYPAY/BPAY IDN */
  
  $idn = null;

  if ($paymentType == 3 || $paymentType == 4)
  {
    require_once('../phpincludes/epay/idn.php');
    
    $idn = requestIdn(array(
        "invoice" => $orderNumber,
        "amount" => number_format($orderTotal, 2),
        "expirationDate" => date("d.m.Y H:i", strtotime("+48 hours")),
    ));
    
    if (!$idn)
    {
        echo json_encode(array("error" => "order-completion", "message" => "Грешка при заявяване на номер за плащане."));
        die(0);
    }
    
    mysqli_real_query($dbConnection, "UPDATE purchases SET idn='$idn' WHERE purchase_id=$orderNumber;");
  }

  /* SEND NOTIFICATION E-MAIL FOR THE ORDER */
      
  require_once('../phpincludes/mails.php');
  
  $rcpInfo = array(
    "orderId"         => $orderNumber,
    "mail"            => $shipping["email"],
    "name"            => $shipping["recipientName"],
    "orderTime"       => strtotime("now"),
    "paymentType"     => $paymentType,
    "phoneNumber"     => $shipping["phoneNumber"],
    "shippingAddress" => $shippingAddress,
    "billingAddress"  => $billingAddress,
    "invoiceInfo"     => $invoiceData,
    "comments"        => $comments,
    "orderDetails"    => $orderDetails,
    "orderValue"      => "$total лв",
    "shipmentPrice"   => number_format($shippingPrice, 2). " лв",
    "orderTotal"      => number_format($orderTotal, 2) . " лв",
    "idn"             => $idn
  );
  
  $orderNotificationMail = createMailWithRecipientInfo($rcpInfo, 'notify');
  sendMail($orderNotificationMail);
  
  $nav = array("delivery", "ePay", "EasyPay", "bPay");
  switch($paymentType) {
    case 1:
      $result = mysqli_query($dbConnection, "SELECT sf_complete_purchase('$reservationId', 255);");
      $row = mysqli_fetch_array($result, MYSQLI_NUM);
      $purchaseId = $row[0];
      
      if ($purchaseId != $orderNumber)
      {
        echo json_encode(array("error" => "order-completion", "message" => "Грешка при приключване на поръчката."));
      }
      else 
      {
        /* Payment on delivery sends a single e-mail only. In order to re-enable second email, uncomment.
        $orderConfirmMail = createMailWithRecipientInfo($rcpInfo, 'confirm');
        sendMail($orderConfirmMail);
        */
        echo json_encode(array("status" => "OK", "nav" => $nav[0]));
      }
      break;
      
    case 2:
      require_once('../phpincludes/epay/algo.php');
      $res = epay_query(array(
        "invoice" => $orderNumber,
        "amount" => number_format($orderTotal, 2),
        "expirationDate" => date("d.m.Y H:i", strtotime("+15 minutes")),
      ));
      $res["PAGE"]="paylogin";
      $responseUrl = "https://creativegg.com/complete.php?p=ePay&orderId=$orderNumber";
      $res["URL_OK"]="$responseUrl&status=OK";
      $res["URL_CANCEL"]="$responseUrl&status=CANCEL";
      
      echo json_encode(array("status" => "OK", "nav" => 'ePay', "data" => $res));
      break;
    case 3:
    case 4:
      echo json_encode(array("status" => "OK", "nav" => $nav[$paymentType - 1]));
      break;
  };
  
?>