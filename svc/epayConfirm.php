<?php
error_reporting(0);

require_once('../phpincludes/epay/algo.php');

$ENCODED  = $_POST['encoded'];
$CHECKSUM = $_POST['checksum'];

$hmac     = epay_hmac('sha1', $ENCODED, $ePaySecret);

if ($CHECKSUM != $hmac)
{
    $remoteAddr = $_SERVER['REMOTE_ADDR'];
    error_log("INVALID CHECKSUM. Request from \"$remoteAddr\".\nECODED=$ENCODED\nCHECKSUM=$CHECKSUM", 0);
    
    echo "ERR=Not valid CHECKSUM\n";
    die(0);
}

$data = base64_decode($ENCODED);
$lines_arr = split("\n", $data);

require_once('../phpincludes/config.php'); 
require_once('../phpincludes/common.php');
require_once('../phpincludes/mails.php');

$dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);

$response_data = '';

foreach ($lines_arr as $line)
{
    if (preg_match("/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/", $line, $regs)) {
        $invoice  = $regs[1];
        $status   = $regs[2];
        if ($status == "PAID")
        {
          $pay_date = $regs[4]; # XXX if PAID
          $stan     = $regs[5]; # XXX if PAID
          $bcode    = $regs[6]; # XXX if PAID
        }
        
        $paymentStatus = 0;
        
        switch($status)
        {
          case "PAID":
            $paymentStatus = 255;
            break;
            
          case "EXPIRED":
            $paymentStatus = 254;
            break;

          case "DENIED":
            $paymentStatus = 253;
            break;
            
          default:
            error_log("Invalid status: $status");
            $response_data .= "INVOICE=$invoice:STATUS=ERR\n";
            break;
        }
        
        if ($paymentStatus == 0)
          continue;

        $result = mysqli_query($dbConnection, "SELECT reservation_id FROM reservations WHERE purchase_id=$invoice");
        if (!$result || is_bool($result))
        {
          error_log("could not get reservation id.");
          $response_data .= "INVOICE=$invoice:STATUS=ERR\n";
          continue;          
        }
        
        if (mysqli_num_rows($result) != 1)
        {
          error_log("result count: 1 != ".mysqli_num_rows($result)." for invoice $invoice");
          mysqli_free_result($result);
          $response_data .= "INVOICE=$invoice:STATUS=NO\n";
          continue;
        }
        
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $reservationId = $row[0];
        
        mysqli_free_result($result);
        
        $result = mysqli_query($dbConnection, 
          "SELECT reserved_products.*, products.name, products.price, (reserved_products.quantity * products.price) as total
           FROM reserved_products
           LEFT JOIN products ON reserved_products.product_id=products.product_id
           WHERE reserved_products.reservation_id='$reservationId';");
           
        if (is_bool($result) || !mysqli_num_rows($result))
        {
          if (!is_bool($result)) mysqli_free_result($result);
          
          $response_data .= "INVOICE=$invoice:STATUS=ERR\n";
          error_log("Order details fail");
          continue;
        }
        
        $orderDetails = array();
        $total = 0;
        
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
          extract($row, EXTR_PREFIX_ALL, "product");
          $total += $product_quantity * $product_price;
          array_push($orderDetails, "$product_quantity x $product_name за " . number_format($product_price, 2) . "лв за брой");
        }

        mysqli_free_result($result);
        
        $result = mysqli_query($dbConnection, "SELECT * FROM purchases WHERE purchase_id=$invoice;");

        if (is_bool($result))
        {
          $response_data .= "INVOICE=$invoice:STATUS=ERR\n";
          error_log("Purchase data retrieval fail 1");
          continue;
        }
        if (mysqli_num_rows($result) != 1)
        {
          mysqli_free_result($result);
          $response_data .= "INVOICE=$invoice:STATUS=NO\n";
          error_log("Purchase data retrieval fail 2");
          continue;
        }
        
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $shippingPrice = 0;
        
        if ($total < $freeDeliveryLimit)
        {
          if (preg_match("/^\\s*(((гр|gr)\\.?)|grad\\s|град\\s)?\\s*(sof(i|y|iy)(a|q)|софия)(\\s+(grad|град))?/iu", $row["shipping_address"]))
            $shippingPrice = $deliveryShortDist;
          else
            $shippingPrice = $deliveryLongDist;
        }

        $rcpInfo = array(
          "orderId"         => $invoice,
          "mail"            => $row["shipping_email"],
          "name"            => $row["shipping_name"],
          "orderTime"       => strtotime("now"),
          "paymentType"     => $row["payment_type"],
          "phoneNumber"     => $row["shipping_phone"],
          "shippingAddress" => $row["shipping_address"],
          "billingAddress"  => $row["billing_address"],
          "invoiceInfo"     => $row["invoice_data"],
          "comments"        => $row["comments"],
          "orderDetails"    => $orderDetails,
          "orderValue"      => "$total лв",
          "shipmentPrice"   => number_format($shippingPrice, 2). " лв",
          "orderTotal"      => number_format($total + $shippingPrice, 2) . " лв",
          "idn"             => $row["idn"]
        );
        
        $result = mysqli_query($dbConnection, "SELECT sf_complete_purchase('$reservationId', $paymentStatus);");
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $purchaseId = $row[0];
        
        mysqli_free_result($result);
        
        if ($purchaseId != $invoice)
        {
          error_log("$purchaseId (purchase id) does not match $invoice (invoice)");
          $response_data .= "INVOICE=$invoice:STATUS=ERR\n";
          continue;          
        }
        
        $response_data .= "INVOICE=$invoice:STATUS=OK\n";
        
        if ($paymentStatus == 255)
        {
          $orderConfirmMail = createMailWithRecipientInfo($rcpInfo, 'confirm');
          sendMail($orderConfirmMail);
        }
    }
}

echo $response_data, "\n";

?>