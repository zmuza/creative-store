<?php
  $requiredFields = array('applicationName', 'userAgent', 'errorMessage', 'page', 'url', 'lineNumber');
  $remoteAddr = $_SERVER['REMOTE_ADDR'];
  
  $data = json_decode(file_get_contents("php://input"), true);
   
  if (!is_array($data))
  {
    error_log("Invalid JSON sent from '$remoteAddr'");
    die(0);
  }
  
  $dataKeys = array_keys($data);

  if (count($dataKeys) != count($requiredFields) || count(array_diff($requiredFields, $dataKeys)) != 0)
  {
    error_log("Missing and/or excess data in JSON sent from '$remoteAddr' : " . file_get_contents("php://input"));
    die(0);
  }
  
  unset($dataKeys);
  
  extract($data, EXTR_PREFIX_ALL | EXTR_REFS, "err");  
  
  error_log("Error [$remoteAddr]:\n\t       app = '$err_applicationName'\n\tuser agent = '$err_userAgent'\n\t   message = '$err_errorMessage'\n\t      page = '$err_page'\n\t       url = '$err_url'\n\t      line = $err_lineNumber\n");
?>