<?php
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (array_key_exists("orderId", $data) && !preg_match('/^[0-9a-zA-Z]*$/', $data["orderId"]))
  {
    echo json_encode(array("error" => "input", "message" => "Подадени са невалидни данни."));
    die(0);
  }
  
  require_once('../phpincludes/config.php'); 
  require_once('../phpincludes/common.php');

  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);

  if (!$dbConnection)
  {
    echo json_encode(array("error" => "database", "message" => "Проблем при връзката с базата данни."));
    die(0);
  }

  $orderId = array_key_exists("orderId", $data) ? $data["orderId"] : null;

  if ($orderId)
    mysqli_query($dbConnection, "CALL sp_clear_reservation('$orderId');");
  
  if (!array_key_exists("products", $data))
  {
    echo json_encode(array("status" => "OK"));
    die(0);
  }

  if (!is_array($data["products"]))
  {
    echo json_encode(array("error" => "input", "message" => "Подадени са невалидни данни."));
    die(0);
  }
  
  $products = $data["products"];

  $items = array();

  foreach ($products as $key=>$product)
  {
    if (!is_int($product["productId"]) 
        || !is_int($product["quantity"])) {
      echo json_encode(array("error" => "input", "message" => "Подадени са невалидни данни за продукт."));
      die(0);
    }
    
    $productQuantity = intval($product["quantity"]);
    if ($productQuantity > 0)
      $items[$product["productId"]] = $productQuantity;
  }
  
  if (!count($items))
  {
      echo json_encode(array("error" => "input", "message" => "Поръчката трябва да съдържа поне един продукт."));
      die(0);
  }
    
  $insertItems = array();    
  foreach ($items as $productId => $quantity)
    array_push($insertItems, "(@rid, $productId, $quantity)");
  
  $insertValues = implode(",", $insertItems);
  
  $query =  "START TRANSACTION;
             set @rid=0;
             SELECT sf_create_new_reservation() INTO @rid;
             INSERT INTO reserved_products (reservation_id, product_id, quantity) VALUES $insertValues;
             
             SELECT sf_reservation_confirm(@rid) as reservation_id;
             COMMIT;
            ";
            
  $state = mysqli_multi_query($dbConnection, $query);
  
  if (!$state || mysqli_connect_errno($dbConnection))
  {
    echo json_encode(array("error" => "database", "message" => "Резервацията е неуспешна."));
    mysqli_close($dbConnection);
    die(0);
  }
  
  while(mysqli_more_results($dbConnection))
  {
    mysqli_next_result($dbConnection);
    $reservationIdResult = mysqli_store_result($dbConnection);
    if (!is_bool($reservationIdResult))
      break;
  }
  
  $reservationId = null;
  if (!is_bool($reservationIdResult))
  {
    $row = mysqli_fetch_array($reservationIdResult, MYSQLI_ASSOC);
    $reservationId = $row["reservation_id"];
    mysqli_free_result($reservationIdResult);
  }
  unset($reservationIdResult);
  
  mysqli_close($dbConnection);  
  
  $response = array("status" => "OK");
  if ($reservationId)
  {
    $response["orderId"] = $reservationId;
  }
  else
  {
    $response["error"] = "input";
    $response["message"] = "Недостатъчни количества от продуктите.";
  }

  echo json_encode($response);

?>