<?php
  $productIds = json_decode(file_get_contents("php://input"), true);
  
  if (!is_array($productIds))
  {
    echo "Error: Invalid JSON.";
    echo json_encode(array());
    die(0);
  }
  
  foreach ($productIds as $prodId)
  {
    if (!is_int($prodId))
    {
      echo "Error: Invalid product id sent: " . $prodId .".";
      die(0);
    }
  }
  
  if (sizeof($productIds) < 1)
  {
    echo json_encode(array());
    die(0);
  }
  
  $queryFilter = implode(",", $productIds);

  require_once('../phpincludes/config.php');
  require_once('../phpincludes/common.php');

  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  
  if (!$dbConnection)
  {
    echo "Error: No DB connection.";
    die(0);
  }
  
  $query = "
    SELECT prods.*, imgs.image, GREATEST(0, (prods.quantity - COALESCE(resv.quantity, 0))) as available
    FROM products as prods
    LEFT JOIN product_images as imgs
    ON prods.product_id = imgs.product_id
    LEFT JOIN (
      SELECT product_id, SUM( quantity ) AS quantity
      FROM reserved_products
      GROUP BY product_id
    ) AS resv ON prods.product_id = resv.product_id
    WHERE imgs.is_primary=1 AND prods.product_id IN ($queryFilter);";

  $productsInfoResult = mysqli_query($dbConnection, $query);

  if (!$productsInfoResult)
  {
    echo "Error: DB error.";
    die(0);
  }

  $productsInfo = array();
  
  while($row = mysqli_fetch_array($productsInfoResult, MYSQLI_ASSOC))
  {
    array_push($productsInfo, array(
    "productId" => intval($row["product_id"]),
    "image" => productImage($row["image"]),
    "url" => $row["url_name"],
    "name" => $row["name"],
    "description" => $row["description"],
    "price" => floatval($row["price"]),
    "available" => min(intval($row["available"]), $maxItemsToBuy)
    ));
  }
  
  echo json_encode($productsInfo);
  mysqli_close($dbConnection);
?>