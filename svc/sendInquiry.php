<?php
  require_once('../phpincludes/config.php');
  require_once('../phpincludes/recaptchalib.php');
  
  $data = json_decode(file_get_contents("php://input"), true);
  
  $requiredFields = array('name','email','inquiry','challenge','response');
  
  if (!isset($data) || !$data)
  {
    echo json_encode(array("error" => "input", "message" => "Липсващи данни относно запитването."));
    die(0);
  }
  
  function missingParameters($required, $data)
  {
    $res = array();
    foreach ($required as $key)
    {
      if (!array_key_exists($key, $data))
        array_push($res, $key);
    }
    
    return $res;
  }
  
  $missing = missingParameters($requiredFields, $data);
  
  if (!empty($missing))
  {
    echo json_encode(array("error" => "missing", "message" => "Подадени непълни данни.", "fields" => $missing));
    die(0);
  }
  unset($missing);
  
  function getPhone($phoneString)
  {
    if (!preg_match('/^\s*(\+?[0-9\- ]*)\s*$/', $phoneString))
      return null;
    
    $phone = preg_replace('/[\- ]+/', '-', trim($phoneString));
    
    return strlen($phone) < 24 ? $phone : null;
  }
  
  function pushIfNot($value, $field, &$array) { if (!$value) array_push($array, $field); return $value ? $value : null; }
  
  $invalid = array();
  
  $name       = pushIfNot(strlen($data["name"]) <= 64 ? $data["name"] : null, "name", $invalid);
  $phone      = array_key_exists("phone", $data) ? pushIfNot(getPhone($data["phone"]), "phone", $invalid) : null;
  $email      = pushIfNot(filter_var($data["email"], FILTER_VALIDATE_EMAIL) ? $data["email"] : null, "email", $invalid);
  
  $textLength = array_key_exists("inquiry", $data) ? strlen($data["inquiry"]) : 0;
  $text       = pushIfNot($textLength >= 10 || $textLength < 2000 ? $data["inquiry"] : null, "inquiry", $invalid);

  $challenge  = pushIfNot(array_key_exists("challenge", $data) ? $data["challenge"] : null, "challenge", $invalid);
  $response   = pushIfNot(array_key_exists("response", $data) ? $data["response"] : null, "response", $invalid);

  if (!empty($invalid))
  {
    echo json_encode(array("error" => "invalid", "message" => "Подадени некоректни данни.", "fields" => $invalid));
    die(0);
  }
  unset($invalid);
  
  $captchaResponse = recaptcha_check_answer($captchaPrivate,$_SERVER["REMOTE_ADDR"],"$challenge","$response");

  if (!$captchaResponse->is_valid)
  {
    echo json_encode(array("error" => "captcha", "message" => "Неправилна Captcha валидация."));
    die(0);
  }
  
  $message = "Име: $name\r\nТел.: $phone\r\ne–mail: $email\r\n\r\nЗапитване:\r\n$text";
  $sent = mail($contactsRecepient, "Запитване от $name", $message, "From: $name <$email>\r\nReplay-To: $name <$email>\r\nMIME-Version: 1.0\r\nContent-type:text/plain;charset=utf-8");
  
  if ($sent)
    echo json_encode(array("status" => "OK"));
  else
    echo json_encode(array("error" => "mail", "message" => "Неуспешно изпращане на запитване. Моля, опитайте по-късно."));
?>