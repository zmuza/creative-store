<?php

  $baseUrl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? "https://" : "http://") . $_SERVER['HTTP_HOST'];

  function ensure($value)
  {
    global $baseUrl;
    if ($value)
      return;
      
    header("Location: $baseUrl" );
    die(0);
  }

  if (isset($_GET["tgt"]) && !empty($_GET["tgt"]) && preg_match('/^(cat|prod)$/', $_GET["tgt"]))
  {
    $target = strtolower($_GET["tgt"]);
  }

  $targetId=0;

  if (isset($_GET["id"]) && !empty($_GET["id"]) && preg_match('/^[1-9][0-9]*$/', $_GET["id"]))
  {
    $targetId = intval($_GET["id"]);
  }

  ensure(isset($target) && isset($targetId));

  require_once('phpincludes/config.php');

  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  ensure($dbConnection);

  if ($target === "prod") {
    $res = mysqli_query($dbConnection,"SELECT url_name FROM products WHERE product_id=$targetId LIMIT 1;");
    ensure($res);

    $row = mysqli_fetch_row($res);
    ensure($row);

    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $baseUrl/" . $row[0]);
    mysqli_free_result($res);
    die(0);
  } 
  else if ($target === "cat") {

    $mapping = array("vsichki", "doma", "kuhniata", "ofisa", "naprednali", "deca", "kartichki", "zeleno");
    ensure($targetId >=0 && $targetId < count($mapping));

    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $baseUrl/" . $mapping[$targetId]);
    die(0);
  }
  
  echo "Target is '$target', id is '$targetId'";
?>