<?php
  require_once('phpincludes/common.php');
  require_once('phpincludes/config.php');
  
  $categorySelectRule = "";
  $categoryJoinRule   = "";
  $categoryFilterRule = "";
  $categoryId = 0;
  if (isset($_GET["id"]) && !empty($_GET["id"]) && preg_match('/^[1-9][0-9]*$/', $_GET["id"]))
  {
    $categoryId = $_GET["id"];
    $categorySelectRule = ", cat.category_id";
    $categoryJoinRule   = "RIGHT JOIN product_category as cat ON prods.product_id=cat.product_id";
    $categoryFilterRule = "AND cat.category_id=$categoryId";
  }
  
  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  ensure($dbConnection, "No DB connection.");
  
  $queries = "SELECT * FROM product_categories WHERE category_id=$categoryId;
  
              SELECT prods.* $categorySelectRule , imgs.image FROM products as prods $categoryJoinRule
              LEFT JOIN product_images as imgs
              ON prods.product_id = imgs.product_id
              WHERE imgs.is_primary=1 $categoryFilterRule
              ORDER BY product_id DESC;";

  $resultSet = mysqli_multi_query($dbConnection, $queries);
  ensure($resultSet, "No DB results.");
 
  $categorySet = mysqli_store_result($dbConnection); mysqli_next_result($dbConnection);
  ensure($categorySet, "Could not load category info.");
  $categoryData = mysqli_fetch_array($categorySet, MYSQLI_ASSOC);
  
  $products = mysqli_store_result($dbConnection);
  ensure($products, "Could not load products.");
  
  $headersText = array(
          "Всичките ни щури подаръци на едно място!",
          "Нещица, които правят дома ви по-уютен, цветен и забавен ...",
          "Умнотии за кухнята, без който се чудим как сме живяли досега :)",
          "Най-забавните помощници на работещия човек.",
          "Обикновени неща, необикновено направени, или креативизъм за напреднали...",
          "Забавни подаръци за малчугани.",
          "Картички"
          );
		  
	$catTit = array(
          "Всички подаръци",
          "Подаръци за дома",
          "Подаръци за кухнята",
          "Подаръци за офиса",
          "Подаръци за напреднали",
          "Подаръци за деца",
          "Картички"
          );
?>

<!-- BEGIN PAGE -->

<?php
      $pageId = "category$categoryId";
      $pageTitle = $catTit[$categoryId];
  	  $metaDescription = $categoryData["meta_description"];
  	  $metaKeywords = $categoryData["meta_keywords"];
      require('phpincludes/header.php');
      if ($categoryId >= 0 && $categoryId < count($headersText))
          echo("  		<h1>$headersText[$categoryId]</h1>");

      unset($categoryId);
?> 
  		<products>
<?php
       while($row = mysqli_fetch_array($products, MYSQLI_ASSOC))
       {
?>
  		  <a href="<?=aref($row["url_name"])?>">
  		    <product>
  		      <img src="<?= productImage($row["image"]) ?>" alt="<?=$row["name"]?>" />
  		      <h1><?= $row["name"] ?></h1>
  		      <p><?= $row["price"] ?> лв</p>
  		    </product>
  		  </a>
<?php
	     }
?>
  		</products>
<?php

  require('phpincludes/bottom.php');
  
  mysqli_close($dbConnection);  
?>