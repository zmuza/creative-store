<?php 
  if (!isset($_GET["id"]) || empty($_GET["id"]) || !preg_match('/^[0-9a-zA-Z]*$/', $_GET["id"]))
  {
    header("Location: ./cart.php");
    die(0);
  }
    
  $reservationId = $_GET["id"];

  require_once('phpincludes/common.php');
  require_once('phpincludes/config.php');
  
  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  ensure($dbConnection, "No DB connection");
  
  $multi = mysqli_multi_query($dbConnection, 
    "CALL sp_get_purchase_for_reservation('$reservationId');
    
     SELECT reserved_products.*, products.name, products.price, (reserved_products.quantity * products.price) as total
     FROM reserved_products
     LEFT JOIN products ON reserved_products.product_id=products.product_id
     WHERE reserved_products.reservation_id='$reservationId';"
  );
    
  $purchaseIdResult = mysqli_store_result($dbConnection); mysqli_next_result($dbConnection);

  $row = mysqli_fetch_array($purchaseIdResult);
  $orderId = $row["purchase_id"];
  mysqli_free_result($purchaseIdResult);

  if (!$orderId)
  {
    header("Location: ./cart.php");
    die(0);
  }  
  
  mysqli_next_result($dbConnection);
  $products = mysqli_store_result($dbConnection);
  ensure($products, "Грешка при четене на данни за поръчката.");
  
  if (mysqli_num_rows($products) == 0)
  {
    header("Location: ./cart.php");
    die(0);
  }
  
  $pageId = 'payment';
?>

<!-- BEGIN PAGE -->

<?php 
      $additionalJS  = array('payment/epay-redirect.js', 'payment/validation.js', 'payment/payment.js');
      $additionalCSS = array('payment.css');
	  $pageTitle = "Creative Egg - Плащане";
      require('phpincludes/header.php');
?>
      <payment>
        <p>Продуктите от Вашата Количка са запазени за Вас в рамките на 15 минути.</p>
        <billing>
          <contact id="delivery_address">
            <h1>Адрес за доставка</h1>
            <p>Лице за контакт</p><input validation="recipientName" name="deliveryName" required autofocus>
            <p>Населено място</p><input id="deliveryMunicipality" validation="municipality" name="deliveryMunicipality" required>
            <p>Адрес</p><input validation="address" name="deliveryAddress" required>
            <p>Телефон за връзка</p><input validation="phoneNumber" name="deliveryPhoneNumber" required>
            <p>e-mail</p><input validation="email" name="deliveryEmail" required>
          </contact>
          <contact id="invoice_info" class="hidden">
            <h1>Данни за фактура</h1>
            <p>Име на фирмата</p><input validation="companyName" name="companyName" />
            <p>Град</p><input validation="municipality" name="companyMunicipality" />
            <p>Адрес на фирмата</p><input validation="address" name="companyAddress" />
            <p>ЕИК/ЕИК по ДДС</p><input validation="unifiedIdNumber" name="unifiedIdNumber" />
            <p>МОЛ</p><input validation="mol" name="companyMol" />
          </contact>
        </billing>
        <div><input id="requireInvoiceCheckbox" type="checkbox" /><label for="requireInvoiceCheckbox">Желая да ми бъде издадена фактура.</label></div>
        <h1>Коментар по поръчката</h1>
        <textarea id="orderComments" rows="4"></textarea>
        <h1>Начин на плащане</h1>
        <payment_options>
          <p class="invalid">Моля, изберете начин на плащане</p>
          <input id="onDelivery" name="paymentType" type="radio" returns="1" /><label for="onDelivery">Наложен платеж</label>
          <p>Заплащате дължимата сума по Вашата поръчка на куриера, при получаване на стоката</p>
          <input id="ePay" name="paymentType" type="radio" returns="2" /><label for="ePay">еPay</label>
          <p>Заплащането се извършва чрез системата на ePay.bg, след регистрация</p>
          <input id="easyPay" name="paymentType" type="radio" returns="3" /><label for="easyPay">EasyPay</label>
          <p>Заплащането се извършва на каса на EasyPay, след представяне на 10 цифрения код за покупка<br />Кодът за плащане ще бъде активен в рамките на 48 часа. Продуктите ще бъдат изпратени само след потвърждение на плащането.</p>
          <input id="bPay" name="paymentType" type="radio" returns="4" /><label for="bPay">През банкомат /bPay/</label>
          <p>Заплащането се извършва чрез банкомат, след въвеждане на 10 цифрения код за покупка<br />Кодът за плащане ще бъде активен в рамките на 48 часа. Продуктите ще бъдат изпратени само след потвърждение на плащането.</p>
        </payment_options>
        <h1 id="summary_head">Поръчка&nbsp;&nbsp;&#35;&nbsp;<?= str_pad($orderId, 6, 0, STR_PAD_LEFT) ?></h1>
        <summary>
          <summary_info>
            <table id="details">
<?php $total = 0;
      while ($row = mysqli_fetch_array($products, MYSQLI_ASSOC)) {
         $total += floatval($row["total"]);
?>
              <tr>
                <td>Продукт <?= $row["name"] ?> х <?= $row["quantity"] ?> бр.</td>
                <td>=</td>
                <td><?= $row["total"] ?>лв.</td>
                <td>(ед.цена <?= $row["price"] ?>лв.)</td>
              </tr>
<?php  }

       $deliveryPriceAttributes = $total < $freeDeliveryLimit ? " id=\"deliveryPrice\" short=\"$deliveryShortDist\" long=\"$deliveryLongDist\"" : "";
       $totalPriceAttributes = $total < $freeDeliveryLimit ? " id=\"totalPrice\" price=\"$total\"" : "";
       $totalPrice = $total + ($total < $freeDeliveryLimit ? $deliveryLongDist : 0);
?>
              <tr>
                <td>Доставка</td>
                <td>=</td>
                <td<?= $deliveryPriceAttributes ?>><?= deliveryPriceText($total, true) ?></td>
              </tr>
            </table>
            <total_price>
              <price_info>
                <price<?= $totalPriceAttributes ?>><?= number_format($totalPrice, 2) ?></price>
                <p>/обща сума за плащане/</p>
              </price_info>
            </total_price>
            <div>
              <button id="order" purl="<?= $epayUrl."/"?>">поръчай</button>
            </div>
          </summary_info>
        </summary>
      </payment>
<?php
  require('phpincludes/bottom.php');
?>