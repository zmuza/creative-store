<?php
      require_once('phpincludes/common.php');
      require_once('phpincludes/config.php');

      $pageId = 'complete';

      $paymentType = $_GET["p"] ? strtolower($_GET["p"]) : null;
?>

<!-- BEGIN PAGE -->

<? 
      $additionalCSS = array('complete.css');
      $pageTitle = "Creative Egg - Благодаря!";
      require('phpincludes/header.php')
?>
<? switch($paymentType)
      {
        case 'delivery': ?>
      <h1>Благодарим Ви, че поръчахте от creativegg.com</h1>
      <info>
        <p>Вие избрахте да заплатите чрез <t>Наложен платеж</t>.</p>
        <p>Плащането се извършва при доставка на поръчката.</p>
        <p style="margin-top: 24px;">Информация за Вашата поръчка ще бъде изпратена на e–mail адреса Ви.</p>
      </info>
<?        break;
        case 'inquiry': ?>
      <h1>Благодарим Ви, че се свързахте с нас.</h1>
      <info>
        <p>Вашето <t>запитване</t> беше изпратено успешно.</p>
      </info>
<?        break;
        case 'epay': ?>
      <h1>Благодарим Ви, че поръчахте от creativegg.com</h1>
      <info>
        <p>Вие избрахте да заплатите през системата на <t>ePay</t>.</p>
      </info>
<?        break;
        case 'easypay': ?>
      <h1>Благодарим Ви, че поръчахте от creativegg.com</h1>
      <info>
        <p>Вие избрахте да заплатите през каса на <t>EasyPay</t>.</p>
        <p>Код за плащане, инструкции и информация за поръчката ще бъдат изпратени на e–mail адреса Ви.</p>
        <notice>
          <p>Кодът за плащане ще бъде активен в рамките на 48 часа.</p>
        </notice>
        <p class="notice">Поръчката ще бъде изпратена само след потвърждение на плащането.</p>
      </info>
<?        break;
        case 'bpay': ?>
      <h1>Благодарим Ви, че поръчахте от creativegg.com</h1>
      <info>
        <p>Вие избрахте да заплатите през <t>банкомат/B–pay/</t>.</p>
        <p>Код за плащане, инструкции и информация за поръчката ще бъдат изпратени на e–mail адреса Ви.</p>
        <notice>
          <p>Кодът за плащане ще бъде активен в рамките на 48 часа.</p>
        </notice>
        <p class="notice">Поръчката ще бъде изпратена само след потвърждение на плащането.</p>
      </info>
<?php
      }
  require('phpincludes/bottom.php');
?>