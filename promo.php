<?php
  require_once('phpincludes/common.php');
  require_once('phpincludes/config.php');
  
  $dbConnection = mysqli_connect($dbHost, $userName, $password, $dbName);
  ensure($dbConnection, "No DB connection.");

  $products = mysqli_query($dbConnection,
    "SELECT prods.*, imgs.image FROM products as prods ".
    "LEFT JOIN product_images as imgs " .
      "ON prods.product_id = imgs.product_id " .
	"RIGHT JOIN promo " . 
	  "ON prods.product_id=promo.product_id " .
    "WHERE imgs.is_primary=1 " .
    "ORDER BY product_id DESC;"
  );
  
  ensure($products, "Could not load products.");

?>

<!-- BEGIN PAGE -->

<?php 
      $pageId = "promo";
  	  $pageTitle = "Промоции";
  	  
  	  if (isset($blackFriday))
        $bodyClass="blackfriday";

      require('phpincludes/header.php');
?> 
      <banner></banner>
  		<products>
<?php
       while($row = mysqli_fetch_array($products, MYSQLI_ASSOC))
       {
?>
  		  <a href="<?=aref($row[url_name])?>">
  		    <product>
  		      <img src="<?= productImage($row["image"]) ?>" />
  		      <p><?= $row["name"] ?></p>
  		      <p><?= $row["price"] ?>лв</p>
  		    </product>
  		  </a>
<?php
	     }
?>
  		</products>
<?php

  require('phpincludes/bottom.php');
  
  mysqli_close($dbConnection);  
?>